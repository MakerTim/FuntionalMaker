package nl.makertim.functionalmaker.annotation;

public class TypeIsNotASingletonException extends RuntimeException {

	public TypeIsNotASingletonException() {
	}

	public TypeIsNotASingletonException(String message) {
		super(message);
	}

	public TypeIsNotASingletonException(Throwable cause) {
		super(cause);
	}
}
