package nl.makertim.functionalmaker.annotation;

@Singleton
public class AdvancedSingletonImpl {

	private BasicSingletonImpl singleton;

	private AdvancedSingletonImpl(@Inject BasicSingletonImpl singleton) {
		this.singleton = singleton;
	}

	public BasicSingletonImpl getSingleton() {
		return singleton;
	}
}
