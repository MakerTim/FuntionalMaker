package nl.makertim.functionalmaker.annotation;

public class BasicInjectNestedImpl extends BasicInjectImpl {

	@Inject
	private static BasicSingletonImpl singleton2;

	static {
		Processor.instance.injectStatic(BasicInjectNestedImpl.class);
	}

	public BasicSingletonImpl getSingleton2() {
		return singleton2;
	}
}
