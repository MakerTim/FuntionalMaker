package nl.makertim.functionalmaker.annotation;

public class InjectParamImpl {

	private BasicSingletonImpl basicSingleton;
	private AdvancedSingletonImpl advancedSingleton;

	public InjectParamImpl(@Inject BasicSingletonImpl basicSingleton, @Inject AdvancedSingletonImpl advancedSingleton) {
		this.basicSingleton = basicSingleton;
		this.advancedSingleton = advancedSingleton;
	}

	public BasicSingletonImpl getBasicSingleton() {
		return basicSingleton;
	}

	public AdvancedSingletonImpl getAdvancedSingleton() {
		return advancedSingleton;
	}
}
