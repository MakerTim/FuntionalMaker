package nl.makertim.functionalmaker.irc.irc;

import nl.makertim.functionalmaker.Try;
import nl.makertim.functionalmaker.collection.MapList;
import nl.makertim.functionalmaker.functionalinterface.EmptyRunnable;
import nl.makertim.functionalmaker.irc.message.*;
import nl.makertim.functionalmaker.property.ObjectProperty;
import nl.makertim.functionalmaker.property.StringProperty;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public abstract class IRC<S extends IRC, M extends Message> implements Runnable {

	protected Socket server;
	protected OutputStream out;
	protected InputStream in;
	protected MessageParser<M> messageParser;

	protected List<String> connectedChannels;
	protected Consumer<Throwable> onError;
	private Runnable onConnect = EmptyRunnable.DO_NOTHING;

	private List<MessageReceiver<M>> globalListeners = new ArrayList<>();
	private List<RawMessageSender> sendEventListeners = new ArrayList<>();
	private List<RawMessageReceiver> receiveEventListeners = new ArrayList<>();
	private MapList<Command, MessageReceiver<M>> listeners = new MapList<>();

	protected IRC(Socket socket, MessageParser<M> messageParser, Consumer<Throwable> onError) {
		this.server = socket;
		this.messageParser = messageParser;
		Try.execute(() -> {
			this.out = server.getOutputStream();
			this.in = server.getInputStream();
		}).onFail(onError);
		this.onError = onError;
		this.connectedChannels = new ArrayList<>();
		registerDefaultListeners();
	}

	protected void registerDefaultListeners() {
		on(Command.PING, message -> {
			pong(message.getContent());
		});
	}

	public S onSend(RawMessageSender listener) {
		sendEventListeners.add(listener);
		return returnThis();
	}

	public S removeOnSend(RawMessageSender listener) {
		sendEventListeners.remove(listener);
		return returnThis();
	}

	public S onReceive(RawMessageReceiver listener) {
		receiveEventListeners.add(listener);
		return returnThis();
	}

	public S removeOnReceive(RawMessageReceiver listener) {
		receiveEventListeners.remove(listener);
		return returnThis();
	}

	public S onGlobal(MessageReceiver<M> receiver) {
		globalListeners.add(receiver);
		return returnThis();
	}

	public S onChat(MessageReceiver<M> receiver) {
		return on(Command.PRIVMSG, receiver);
	}

	public S onJoin(MessageReceiver<M> receiver) {
		return on(Command.JOIN, receiver);
	}

	public S on(Command channel, MessageReceiver<M> receiver) {
		listeners.append(channel, receiver);
		return returnThis();
	}

	public S removeOnGlobal(MessageReceiver receiver) {
		globalListeners.remove(receiver);
		return returnThis();
	}

	public S removeOn(Command channel, MessageReceiver<M> receiver) {
		listeners.cutoff(channel, receiver);
		return returnThis();
	}

	// Called on connecting to the server
	public abstract S login(String username, String nickname, String auth);

	// Called after login
	protected void onConnect() {
		onConnect.run();
	}

	public void setOnConnect(Runnable onConnect) {
		this.onConnect = onConnect;
	}

	public S joinChannel(String channel) {
		sendRaw(String.format("JOIN %s", channel));
		connectedChannels.add(channel);
		return returnThis();
	}

	public S broadcast(String text) {
		connectedChannels.forEach(channel -> {
			privateMessage(channel, text);
		});
		return returnThis();
	}

	public S sendMessage(String to, String text) {
		return privateMessage(to, text);
	}

	public S privateMessage(String to, String text) {
		sendRaw(String.format("PRIVMSG %s :%s", to, text));
		return returnThis();
	}

	public S pong(String server) {
		sendRaw(String.format("PONG %s", server));
		return returnThis();
	}

	public S leaveChannel(String channel) {
		return part(channel);
	}

	public S part(String channel) {
		sendRaw(String.format("PART %s", channel));
		connectedChannels.remove(channel);
		return returnThis();
	}

	public S quit(String reason) {
		sendRaw(String.format("QUIT %s", reason));
		connectedChannels.clear();
		return returnThis();
	}

	protected S receiveMessage(M message) {
		globalListeners
				.forEach(listener ->
						Try.execute(() -> listener.accept(message))
								.onFail(onError)
				);
		listeners.getOrDefault(message.getCommand(), new ArrayList<>())
				.forEach(listener ->
						Try.execute(() -> listener.accept(message))
								.onFail(onError)
				);
		return returnThis();
	}

	@Deprecated // Should not be used
	public S rawSend(String text) {
		return sendRaw(text);
	}

	protected S sendRaw(String text) {
		ObjectProperty<byte[]> bytesProperty = new ObjectProperty<>((text + "\r\n").getBytes());
		sendEventListeners.forEach(listener -> {
			bytesProperty.setValue(listener.apply(bytesProperty.get()));
		});
		Try.execute(() -> out.write(bytesProperty.get()));
		return returnThis();
	}

	public void startListeningThreaded() {
		new Thread(this).start();
	}

	public void startListening() {
		run();
	}

	@Override
	public void run() {
		Try.execute(() -> {
			MessageBuffer messageBuffer = new MessageBuffer();
			byte[] buffer = new byte[2048];
			int count;

			while (true) {
				count = in.read(buffer);
				if (count == -1)
					break;
				messageBuffer.append(Arrays.copyOfRange(buffer, 0, count));
				while (messageBuffer.hasCompleteMessage()) {
					StringProperty ircMessageProperty = new StringProperty(messageBuffer.getNextMessage());
					receiveEventListeners.forEach(listener -> {
						ircMessageProperty.setValue(listener.apply(ircMessageProperty.get()));
					});
					receiveMessage(messageParser.parse(ircMessageProperty.get()));
				}
			}
		}).onFail(onError);
	}

	protected abstract S returnThis();
}
