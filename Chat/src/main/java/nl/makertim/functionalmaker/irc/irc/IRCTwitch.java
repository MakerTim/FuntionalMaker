package nl.makertim.functionalmaker.irc.irc;

import nl.makertim.functionalmaker.irc.message.TwitchMessage;
import nl.makertim.functionalmaker.irc.message.TwitchMessageParser;

import java.net.Socket;
import java.util.function.Consumer;

public class IRCTwitch extends IRC<IRCTwitch, TwitchMessage> {

	public IRCTwitch(Socket socket, Consumer<Throwable> onError) {
		super(socket, new TwitchMessageParser(), onError);
	}

	@Override
	protected void registerDefaultListeners() {
		super.registerDefaultListeners();
		on(Command.RPL_ENDOFMOTD, message -> {
			onConnect();
		});
		on(Command.MODE, message -> {
			TwitchMessage joinMessage = message.copy();
			joinMessage.setRawCommand("JOIN");
			receiveMessage(joinMessage);
		});
	}

	@Override
	public IRCTwitch login(String username, String nickname, String auth) {
		sendRaw(String.format("PASS %s", auth));
		sendRaw(String.format("NICK %s", username));
		return returnThis();
	}

	@Override
	public IRCTwitch privateMessage(String to, String text) {
		if (!to.startsWith("#")) {
			return super.privateMessage("#jtv", String.format("/w %s %s", to, text));
		}
		return super.privateMessage(to, text);
	}

	@Override
	protected void onConnect() {
		super.onConnect();
		sendRaw("CAP REQ :twitch.tv/membership");
		sendRaw("CAP REQ :twitch.tv/commands");
		sendRaw("CAP REQ :twitch.tv/tags");
	}

	@Override
	protected IRCTwitch returnThis() {
		return this;
	}
}
