package nl.makertim.functionalmaker.irc.message;

import nl.makertim.functionalmaker.ArraysPlus;
import nl.makertim.functionalmaker.property.IntProperty;

import java.util.Arrays;

public abstract class AbstractMessageParser<M extends Message> implements MessageParser<M> {

	protected abstract M newMessage();

	@Override
	public M parse(String ircMessage) {
		String[] messageParts = ircMessage.split(" ");
		M message = newMessage();
		message.setRaw(ircMessage);
		if (messageParts.length == 0) {
			return message;
		}

		IntProperty indexProperty = new IntProperty(0);

		parsePre(message, messageParts, indexProperty);
		parseOrigin(message, messageParts, indexProperty);
		if (messageParts.length == indexProperty.getValue()) {
			message.setRawCommand("null");
			return message;
		}

		parseCommand(message, messageParts, indexProperty);
		parseContent(message, messageParts, indexProperty);
		return message;
	}

	protected void parsePre(M message, String[] messageParts, IntProperty indexProperty) {
	}

	protected void parseOrigin(Message message, String[] messageParts, IntProperty indexProperty) {
		String part = messageParts[indexProperty.getValue()];
		if (part.startsWith(":")) {
			indexProperty.addOne();
			String origin = part.substring(1);
			message.setOrigin(origin);

			int usernameIndex = origin.indexOf('!');
			if (usernameIndex >= 0) {
				message.setNickname(origin.substring(0, usernameIndex));
			}
		}
	}

	protected void parseCommand(Message message, String[] messageParts, IntProperty indexProperty) {
		String cmd = messageParts[indexProperty.getValue()].toLowerCase();
		indexProperty.addOne();
		if (cmd.startsWith(":")) {
			cmd = cmd.substring(1);
		}
		message.setRawCommand(cmd);
	}

	protected void parseContent(Message message, String[] messageParts, IntProperty indexProperty) {
		String[] msgGrouped = Arrays.copyOfRange(messageParts, Math.min(messageParts.length, indexProperty.getValue()), messageParts.length);
		String msg = ArraysPlus.toString(" ", msgGrouped);
		if (msg.startsWith(":")) {
			msg = msg.substring(1);
		}
		message.setContent(msg);
	}

	protected void parsePost(M message, String[] messageParts, IntProperty indexProperty) {
	}
}
