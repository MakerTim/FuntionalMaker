package nl.makertim.functionalmaker.irc.message;

public interface MessageParser<M extends Message> {

	M parse(String ircMessage);
}
