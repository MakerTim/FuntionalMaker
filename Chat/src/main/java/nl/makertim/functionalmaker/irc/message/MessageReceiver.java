package nl.makertim.functionalmaker.irc.message;

import java.util.function.Consumer;

public interface MessageReceiver<M extends Message> extends Consumer<M> {

}
