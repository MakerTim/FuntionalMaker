package nl.makertim.functionalmaker.irc.message;

import java.util.function.UnaryOperator;

public interface RawMessageReceiver extends UnaryOperator<String> {
}
