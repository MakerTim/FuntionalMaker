package nl.makertim.functionalmaker.irc.message;

import nl.makertim.functionalmaker.irc.message.TwitchMessage.Badge;
import nl.makertim.functionalmaker.property.IntProperty;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class TwitchMessageParser extends AbstractMessageParser<TwitchMessage> {

	@Override
	protected TwitchMessage newMessage() {
		return new TwitchMessage();
	}

	@Override
	protected void parsePre(TwitchMessage message, String[] messageParts, IntProperty indexProperty) {
		String tagsString = messageParts[indexProperty.getValue()];
		if (tagsString.startsWith("@")) {
			indexProperty.addOne();
			message.setRawTags(tagsString);
			tagsString = tagsString.substring(1);
			String[] tags = tagsString.split(";");
			Map<String, String> tagsMap = new HashMap<>();
			Arrays.stream(tags).forEach(tag -> {
				String[] keyValue = tag.split("=", 2);
				tagsMap.put(keyValue[0], keyValue[1]);
			});

			message.setRawTagsMap(tagsMap);
			message.setBadges(Arrays.stream(tagsMap.getOrDefault("badges", "").split(","))
					.map((Function<String, Object>) Badge::fromString)
					.toArray(Badge[]::new));
			message.setBits(Integer.parseInt(tagsMap.getOrDefault("bits", "-1")));
			message.setLanguage("broadcaster-lang");
			message.setColor(tagsMap.getOrDefault("color", "#000000"));
			message.setDisplayName(tagsMap.get("display-name"));
			message.setUserId(tagsMap.get("user-id"));
			message.setRoomId(tagsMap.get("room-id"));
			message.setMessageId(tagsMap.get("id"));
			message.setType(tagsMap.get("user-type"));
			message.setMod(tagsMap.getOrDefault("mod", "0").equals("1"));
			message.setSub(tagsMap.getOrDefault("subscriber", "0").equals("1"));
			message.setTurbo(tagsMap.getOrDefault("badges", "0").equals("1"));
		}
	}
}
