package nl.makertim.functionalmaker.irc;

import nl.makertim.functionalmaker.ConsumerListener;
import nl.makertim.functionalmaker.irc.irc.IRC;
import nl.makertim.functionalmaker.irc.irc.IRCTwitch;
import nl.makertim.functionalmaker.irc.irc.IRCVanilla;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

import static nl.makertim.functionalmaker.irc.IRCConnection.connectIRC;

public class TestConnect {

	@Test
	public void testVanillaConnect() {
		ConsumerListener<Throwable> errorListener = new ConsumerListener<>();
		Optional<IRCVanilla> optionalIRC = connectIRC(IRCVanilla.class, "halcyon.il.us.dal.net", 6667, errorListener.andThen(Throwable::printStackTrace));
		optionalIRC.ifPresent(client -> {
			setupDebug(client);
			client.login("MakerTimIRC", "MakerTimIRC", "null");
			client.setOnConnect(() -> {
				client.joinChannel("#makertim");
				client.privateMessage("#makertim", "This is a automated test.");
				client.quit("Testing done");
			});
			client.startListening();
		});

		Assert.assertTrue(!errorListener.isCalled());
	}

	@Test
	public void testTwitchConnect() {
		ConsumerListener<Throwable> errorListener = new ConsumerListener<>();
		Optional<IRCTwitch> optionalIRC = connectIRC(IRCTwitch.class, "irc.chat.twitch.tv", 6667, errorListener.andThen(Throwable::printStackTrace));
		optionalIRC.ifPresent(client -> {
			client.login("MakerTimBot", "MakerTimBot", "oauth:fnhkepfvxw55vwmlq68t1t40y2inyb");
			client.setOnConnect(() -> {
				setupDebug(client);
				client.joinChannel("#makertim");
				client.privateMessage("#makertim", "This is a automated test.");
				client.quit("byebye");
			});
			client.startListening();
		});

		Assert.assertTrue(!errorListener.isCalled());
	}

	private void setupDebug(IRC client) {
		client.onSend(sending -> {
			System.out.println("> " + new String(sending));
			return sending;
		});
		client.onReceive(receiving -> {
			System.out.println("< " + receiving);
			return receiving;
		});
		client.onGlobal(message -> System.out.println("\t\t" + message));
	}
}
