package nl.makertim.functionalmaker;

public class ArraysPlus {

	private ArraysPlus() {
	}

	@SafeVarargs
	public static <T> T[] of(T... of) {
		return of;
	}

	public static String toString(String delimiter, String... objects) {
		return toString(delimiter, (Object[]) objects);
	}

	@SafeVarargs
	public static <T> String toString(String delimiter, T... objects) {
		if (objects.length == 0) {
			return "";
		}
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < objects.length - 1; i++) {
			builder.append(objects[i].toString())
					.append(delimiter);
		}
		builder.append(objects[objects.length - 1].toString());
		return builder.toString();
	}
}
