package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.functionalinterface.CatchRunnable;
import nl.makertim.functionalmaker.property.IntProperty;

public class AssertTry {

	private AssertTry() {
	}

	public static <E extends Throwable> void assumeThrows(CatchRunnable<E> runnable, Class<E> clazz) {
		assumeThrows("assumeThrows didn't throw " + clazz.getName(), runnable, clazz);
	}

	public static <E extends Throwable> void assumeThrows(String why, CatchRunnable<E> runnable, Class<E> clazz) {
		Try.execute(runnable::run).onFail(thr -> {
			if (!clazz.isInstance(thr)) {
				throw new AssertionError(why);
			}
		});
	}

	public static void assumeSafe(CatchRunnable<Throwable> runnable) {
		assumeSafe("assumeSafe throws {}", runnable);
	}

	public static <E extends Throwable> void assumeSafe(String why, CatchRunnable<E> runnable) {
		Try.execute(runnable::run).onFail(thr -> {
			throw new AssertionError(why.replace("{}", thr.toString()));
		});
	}

	public static void any(CatchRunnable<?>... runnable) {
		any("None of the runs completed without errors", runnable);
	}

	public static void any(String why, CatchRunnable<?>... runnable) {
		int timesPassed = timesPassed(runnable);
		if (timesPassed <= 0) {
			throw new AssertionError(why);
		}
	}

	public static void or(CatchRunnable<Throwable> runnable1, CatchRunnable<Throwable> runnable2) {
		or("None matched of both runnables", runnable1, runnable2);
	}

	public static void or(String why, CatchRunnable<Throwable> runnable1, CatchRunnable<Throwable> runnable2) {
		int timesPassed = timesPassed(runnable1, runnable2);
		if (timesPassed <= 0) {
			throw new AssertionError(why);
		}
	}

	public static void xor(CatchRunnable<Throwable> runnable1, CatchRunnable<Throwable> runnable2) {
		xor("{} matched instead of one of the two", runnable1, runnable2);
	}

	public static void xor(String why, CatchRunnable<Throwable> runnable1, CatchRunnable<Throwable> runnable2) {
		int timesPassed = timesPassed(runnable1, runnable2);
		if (timesPassed != 1) {
			throw new AssertionError(why.replace("{}", timesPassed == 0 ? "None" : "Both"));
		}
	}

	public static int timesPassed(CatchRunnable<?>... runnable) {
		IntProperty passedProperty = new IntProperty(0);
		for (CatchRunnable<?> run : runnable) {
			Try.execute(run).then(passedProperty::addOne);
		}
		return passedProperty.getValue();
	}
}
