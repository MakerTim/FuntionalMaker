package nl.makertim.functionalmaker;

import java.util.function.Consumer;

public class ConsumerListener<T> implements Consumer<T> {

	private boolean called = false;

	public boolean isCalled() {
		return called;
	}

	@Override
	public void accept(T o) {
		called = true;
	}
}
