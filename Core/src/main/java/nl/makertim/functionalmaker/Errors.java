package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.functionalinterface.*;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.function.*;

@SuppressWarnings("unchecked")
public class Errors<E extends Throwable> {

	protected Consumer<E> handler;
	private Class<E> clazz;


	private final Consumer<Throwable> errorBehaviour = thr -> {
		if (clazz.isInstance(thr)) {
			handler.accept((E) thr);
		} else {
			throw new NestedException(thr);
		}
	};

	public static Errors<Exception> ignore() {
		return ignore(Exception.class);
	}

	public static <E extends Throwable> Errors<E> ignore(Class<E> cls) {
		return new Errors<>(err -> {
		}, cls);
	}

	public static Errors<Exception> log(PrintStream out) {
		return log(out, Exception.class);
	}

	public static <E extends Throwable> Errors<E> log(PrintStream out, Class<E> cls) {
		return new Errors<>(out::println, cls);
	}

	public static Errors<Exception> runtime() {
		return runtime(Exception.class);
	}

	public static <E extends Throwable> Errors<E> runtime(Class<E> cls) {
		return new Errors<>(e -> {
			throw new NestedException(e);
		}, cls);
	}

	public static Errors<Exception> self(Consumer<Exception> consumer) {
		return self(consumer, Exception.class);
	}

	public static <E extends Throwable> Errors<E> self(Consumer<E> consumer, Class<E> cls) {
		return new Errors<>(consumer, cls);
	}

	private Errors(Consumer<E> onError, Class<E> cls) {
		handler = onError;
		clazz = cls;
	}


	public <T> Consumer<T> accept(CatchConsumer<T, E> consumer) {
		return t -> {
			try {
				consumer.accept(t);
			} catch (Throwable thr) {
				if (clazz.isInstance(thr)) {
					handler.accept((E) thr);
				} else {
					throw new NestedException(thr);
				}
			}
		};
	}


	public <T, R> Function<T, R> apply(CatchFunction<T, R, E> function) {
		return t -> {
			try {
				return function.apply(t);
			} catch (Throwable thr) {
				if (clazz.isInstance(thr)) {
					handler.accept((E) thr);
					return null;
				} else {
					throw new NestedException(thr);
				}
			}
		};
	}


	public <T> ToIntFunction<T> applyAsInt(CatchToIntFunction<T, E> function) {
		return t -> {
			try {
				return function.applyAsInt(t);
			} catch (Throwable thr) {
				if (clazz.isInstance(thr)) {
					handler.accept((E) thr);
					return -1;
				} else {
					throw new NestedException(thr);
				}
			}
		};
	}


	public <T> ToLongFunction<T> applyAsLong(CatchToLongFunction<T, E> function) {
		return t -> {
			try {
				return function.applyAsLong(t);
			} catch (Throwable thr) {
				if (clazz.isInstance(thr)) {
					handler.accept((E) thr);
					return -1;
				} else {
					throw new NestedException(thr);
				}
			}
		};
	}


	public <T> ToDoubleFunction<T> applyAsDouble(CatchToDoubleFunction<T, E> function) {
		return t -> {
			try {
				return function.applyAsDouble(t);
			} catch (Throwable thr) {
				if (clazz.isInstance(thr)) {
					handler.accept((E) thr);
					return -1;
				} else {
					throw new NestedException(thr);
				}
			}
		};
	}


	public <T> Comparator<T> compare(CatchComparator<T, E> comparator) {
		return (c1, c2) -> {
			return Try.execute(() -> comparator.compare(c1, c2))
					.onFail(errorBehaviour)
					.orElse(() -> 0)
					.getData();
		};
	}


	public Runnable run(CatchRunnable<E> runnable) {
		return () -> {
			Try.execute(runnable).onFail(errorBehaviour);
		};
	}


	public <T> Predicate<T> test(CatchPredicate<T, E> predicate) {
		return t -> {
			return Try.execute(() -> predicate.test(t))
					.onFail(errorBehaviour)
					.orElse(() -> false)
					.getData();
		};
	}

}
