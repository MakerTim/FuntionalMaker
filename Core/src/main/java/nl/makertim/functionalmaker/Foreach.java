package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.functionalinterface.QuadConsumer;
import nl.makertim.functionalmaker.functionalinterface.TriConsumer;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Foreach {

	private Foreach() {
	}

	@SuppressWarnings("unchecked")
	private static <T> T[] toArray(Collection<T> collection) {
		return (T[]) collection.toArray();
	}

	private static <T> T[] toArray(Iterator<T> collection) {
		List<T> list = new ArrayList<>();
		collection.forEachRemaining(list::add);
		return toArray(list);
	}

	public static <T> void loop(T[] collection, Consumer<T> action) {
		Arrays.stream(collection).forEach(action);
	}

	public static <T> void loop(Collection<T> collection, Consumer<T> action) {
		loop(toArray(collection), action);
	}

	public static <T> void loop(Iterator<T> collection, Consumer<T> action) {
		loop(toArray(collection), action);
	}

	public static <T> void reversed(T[] collection, Consumer<T> action) {
		for (int i = collection.length - 1; i >= 0; i--) {
			action.accept(collection[i]);
		}
	}

	public static <T> void reversed(Collection<T> collection, Consumer<T> action) {
		loop(toArray(collection), action);
	}

	public static <T> void reversed(Iterator<T> collection, Consumer<T> action) {
		loop(toArray(collection), action);
	}

	public static <T> void loopTwice(T[] collection, BiConsumer<T, T> action) {
		Arrays.stream(collection).forEach(t1 ->
				Arrays.stream(collection).forEach(t2 ->
						action.accept(t1, t2)));
	}

	public static <T> void loopTwice(Collection<T> collection, BiConsumer<T, T> action) {
		loopTwice(toArray(collection), action);
	}

	public static <T> void loopTwice(Iterator<T> collection, BiConsumer<T, T> action) {
		loopTwice(toArray(collection), action);
	}

	public static <T> void loopThrice(T[] collection, TriConsumer<T, T, T> action) {
		Arrays.stream(collection).forEach(t1 ->
				Arrays.stream(collection).forEach(t2 ->
						Arrays.stream(collection).forEach(t3 ->
								action.accept(t1, t2, t3))));
	}

	public static <T> void loopThrice(Collection<T> collection, TriConsumer<T, T, T> action) {
		loopThrice(toArray(collection), action);
	}

	public static <T> void loopThrice(Iterator<T> collection, TriConsumer<T, T, T> action) {
		loopThrice(toArray(collection), action);
	}

	public static <T> void loopQuad(T[] collection, QuadConsumer<T, T, T, T> action) {
		Arrays.stream(collection).forEach(t1 ->
				Arrays.stream(collection).forEach(t2 ->
						Arrays.stream(collection).forEach(t3 ->
								Arrays.stream(collection).forEach(t4 ->
										action.accept(t1, t2, t3, t4)
								)
						)
				)
		);
	}

	public static <T> void loopQuad(Collection<T> collection, QuadConsumer<T, T, T, T> action) {
		loopQuad(toArray(collection), action);
	}

	public static <T> void loopQuad(Iterator<T> collection, QuadConsumer<T, T, T, T> action) {
		loopQuad(toArray(collection), action);
	}

	public static <T> void loopOrdered(T[] collection, Consumer<T> action) {
		for (int a = 0; a < collection.length; a++) {
			action.accept(collection[a]);
		}
	}

	public static <T> void loopOrdered(Collection<T> collection, Consumer<T> action) {
		loopOrdered(toArray(collection), action);
	}

	public static <T> void loopOrdered(Iterator<T> collection, Consumer<T> action) {
		loopOrdered(toArray(collection), action);
	}

	public static <T> void loopTwiceOrdered(T[] collection, BiConsumer<T, T> action) {
		for (int order = 0; order < collection.length; order++) {
			for (int a = 0; a < collection.length; a++) {
				for (int b = 0; b < collection.length; b++) {
					if (Math.max(a, b) == order) {
						action.accept(collection[a], collection[b]);
					}
				}
			}
		}
	}

	public static <T> void loopTwiceOrdered(Collection<T> collection, BiConsumer<T, T> action) {
		loopTwiceOrdered(toArray(collection), action);
	}

	public static <T> void loopTwiceOrdered(Iterator<T> collection, BiConsumer<T, T> action) {
		loopTwiceOrdered(toArray(collection), action);
	}

	public static <T> void loopThriceOrdered(T[] collection, TriConsumer<T, T, T> action) {
		for (int order = 0; order < collection.length; order++) {
			for (int a = 0; a < collection.length; a++) {
				for (int b = 0; b < collection.length; b++) {
					for (int c = 0; c < collection.length; c++) {
						if (Math.max(Math.max(a, b), c) == order) {
							action.accept(collection[a], collection[b], collection[c]);
						}
					}
				}
			}
		}
	}

	public static <T> void loopThriceOrdered(Collection<T> collection, TriConsumer<T, T, T> action) {
		loopThriceOrdered(toArray(collection), action);
	}

	public static <T> void loopThriceOrdered(Iterator<T> collection, TriConsumer<T, T, T> action) {
		loopThriceOrdered(toArray(collection), action);
	}

	public static <T> void loopQuadOrdered(T[] collection, QuadConsumer<T, T, T, T> action) {
		for (int order = 0; order < collection.length; order++) {
			for (int a = 0; a < collection.length; a++) {
				for (int b = 0; b < collection.length; b++) {
					for (int c = 0; c < collection.length; c++) {
						for (int d = 0; d < collection.length; d++) {
							if (Math.max(Math.max(Math.max(a, b), c), d) == order) {
								action.accept(collection[a], collection[b], collection[c], collection[d]);
							}
						}
					}
				}
			}
		}
	}

	public static <T> void loopQuadOrdered(Collection<T> collection, QuadConsumer<T, T, T, T> action) {
		loopQuadOrdered(toArray(collection), action);
	}

	public static <T> void loopQuadOrdered(Iterator<T> collection, QuadConsumer<T, T, T, T> action) {
		loopQuadOrdered(toArray(collection), action);
	}
}
