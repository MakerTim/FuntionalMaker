package nl.makertim.functionalmaker;

import java.io.PrintStream;
import java.io.PrintWriter;

public class NestedException extends RuntimeException {

	private final Throwable parent;

	public NestedException(String message, Throwable cause) {
		super(message, cause);
		this.parent = cause;
	}

	public NestedException(Throwable cause) {
		super(cause);
		this.parent = cause;
	}

	public NestedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.parent = cause;
	}

	@Override
	public String getMessage() {
		return parent.getMessage();
	}

	@Override
	public String getLocalizedMessage() {
		return parent.getLocalizedMessage();
	}

	@Override
	public synchronized Throwable getCause() {
		return parent;
	}

	@Override
	public synchronized Throwable initCause(Throwable cause) {
		return parent.initCause(cause);
	}

	@Override
	public void printStackTrace() {
		parent.printStackTrace();
	}

	@Override
	public void printStackTrace(PrintStream s) {
		parent.printStackTrace(s);
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		parent.printStackTrace(s);
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return parent.fillInStackTrace();
	}

	@Override
	public StackTraceElement[] getStackTrace() {
		return parent.getStackTrace();
	}

	@Override
	public void setStackTrace(StackTraceElement[] stackTrace) {
		parent.setStackTrace(stackTrace);
	}

	@Override
	public String toString() {
		return parent.toString();
	}
}
