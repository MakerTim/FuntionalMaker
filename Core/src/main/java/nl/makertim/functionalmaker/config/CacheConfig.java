package nl.makertim.functionalmaker.config;

import java.util.Map;
import java.util.TreeMap;

public class CacheConfig extends Config {

	private Map<String, String> cache = new TreeMap<>();

	@Override
	public void clear() {
		cache.clear();
	}

	@Override
	public String[] getKeys() {
		return cache.keySet().toArray(new String[0]);
	}

	@Override
	protected String getInternally(String key) {
		return cache.get(key);
	}

	@Override
	protected void setInternally(String key, String rawValue) {
		cache.put(key, rawValue);
	}
}
