package nl.makertim.functionalmaker.config;

import nl.makertim.functionalmaker.Try;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

public class CachedFileConfig extends FileConfig {

	private Map<String, String> cache = new TreeMap<>();

	public CachedFileConfig(File file) {
		this(file.toURI());
	}

	public CachedFileConfig(String path) {
		this(new File(path));
	}

	public CachedFileConfig(URI uri) {
		super(uri);
		read();
	}

	@Override
	public void clear() {
		super.clear();
		cache.clear();
	}

	private void read() {
		Try.execute(() -> {
			Path path = Paths.get(uri);
			path.toFile().createNewFile();
			Files.lines(path)
					.forEach(line -> {
						int index = line.indexOf('=');
						cache.put(line.substring(0, index), line.substring(index + 1));
					});
		}).orThrow(RuntimeException::new);
	}

	public void save() {
		Try.execute(() -> {
			List<String> lines = cache.entrySet().stream()
					.map(entry -> entry.getKey() + "=" + entry.getValue())
					.collect(Collectors.toList());
			Files.write(Paths.get(uri), lines, UTF_8, TRUNCATE_EXISTING, CREATE);
		}).orThrow(RuntimeException::new);
	}

	@Override
	public String[] getKeys() {
		return cache.keySet().toArray(new String[0]);
	}

	@Override
	protected String getInternally(String key) {
		return cache.get(key);
	}

	@Override
	protected void setInternally(String key, String rawValue) {
		cache.put(key, rawValue);
	}
}
