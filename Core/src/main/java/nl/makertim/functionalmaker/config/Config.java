package nl.makertim.functionalmaker.config;

import nl.makertim.functionalmaker.Try;

import static nl.makertim.functionalmaker.config.Config.Type.*;

public abstract class Config {

	public enum Type {
		STRING('s'), INTEGER('i'), DOUBLE('d'), LONG('l'), UUID('u'), CUSTOM('c');

		private char representation;

		Type(char representation) {
			this.representation = representation;
		}
	}

	public abstract String[] getKeys();

	public abstract void clear();

	protected abstract String getInternally(String key);

	protected <T> T get(String key, Type type, SerializationFactory<T> factory) {
		String rawGet = getInternally(key);
		if (rawGet == null || rawGet.isEmpty()) {
			return null;
		}
		String get = rawGet.replaceAll("\\n", "\n").replaceAll("\\r", "\r");
		if (get.charAt(0) != type.representation) {
			return null;
		}
		return Try.execute(() -> factory.serialize(get.substring(1))).onFail(Throwable::printStackTrace).orThrow(RuntimeException::new).getData();
	}

	public <T> T getObject(String key, SerializationFactory<T> factory) {
		String get = getInternally(key);
		if (get == null || get.isEmpty()) {
			return null;
		}
		if (get.charAt(0) != CUSTOM.representation) {
			return null;
		}
		return Try.execute(() -> factory.serialize(get.substring(1))).onFail(Throwable::printStackTrace).orThrow(RuntimeException::new).getData();
	}

	public String getString(String key) {
		return get(key, STRING, raw -> raw);
	}

	public int getInt(String key) {
		return get(key, INTEGER, Integer::parseInt);
	}

	public double getDouble(String key) {
		return get(key, DOUBLE, Double::parseDouble);
	}

	public long getLong(String key) {
		return get(key, LONG, Long::parseLong);
	}

	public java.util.UUID getUUID(String key) {
		return get(key, UUID, java.util.UUID::fromString);
	}

	protected abstract void setInternally(String key, String rawValue);

	protected void set(String key, String raw, Type type) {
		String set = type.representation + raw;
		setInternally(key, set.replaceAll("\n", "\\n").replaceAll("\r", "\\r"));
	}

	public <T> void setObject(String key, T object, DeserializationFactory<T> factory) {
		set(key, factory.deserialize(object), CUSTOM);
	}

	public <T extends DeserializationFactory<T>> void setObject(String key, T object) {
		set(key, object.deserialize(object), CUSTOM);
	}

	public void setString(String key, String object) {
		set(key, object, STRING);
	}

	public void setInt(String key, int object) {
		set(key, Integer.toString(object), INTEGER);
	}

	public void setDouble(String key, double object) {
		set(key, Double.toString(object), DOUBLE);
	}

	public void setLong(String key, long object) {
		set(key, Long.toString(object), LONG);
	}

	public void setUUID(String key, java.util.UUID object) {
		set(key, object.toString(), UUID);
	}
}
