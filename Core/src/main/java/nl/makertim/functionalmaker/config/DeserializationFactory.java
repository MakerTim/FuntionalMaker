package nl.makertim.functionalmaker.config;

public interface DeserializationFactory<T> {

	String deserialize(T object);
}
