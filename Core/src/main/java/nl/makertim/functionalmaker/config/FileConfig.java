package nl.makertim.functionalmaker.config;

import nl.makertim.functionalmaker.Try;
import nl.makertim.functionalmaker.property.BooleanProperty;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileConfig extends Config {

	protected URI uri;

	public FileConfig(File file) {
		this(file.toURI());
	}

	public FileConfig(String path) {
		this(new File(path));
	}

	public FileConfig(URI uri) {
		this.uri = uri;
	}

	@Override
	public void clear() {
		Try.execute(() -> Files.write(Paths.get(uri), Collections.emptyList()))
				.orThrow(RuntimeException::new);
	}

	@Override
	public String[] getKeys() {
		return Try.execute(() -> {
			return Files.lines(Paths.get(uri))
					.map(line -> line.substring(0, line.indexOf('=')))
					.toArray(String[]::new);
		}).orElse(() -> new String[0]).getData();
	}

	@Override
	protected String getInternally(String rawKey) {
		String key = rawKey + "=";
		return Try.execute(() -> {
			return Files.lines(Paths.get(uri))
					.filter(line -> line.startsWith(key))
					.findFirst()
					.map(line -> line.substring(key.length()))
					.orElse(null);
		}).getData();
	}

	@Override
	protected void setInternally(String rawKey, String rawValue) {
		String key = rawKey + "=";
		String newLine = key + rawValue;
		Try.execute(() -> {
			BooleanProperty alreadyExisted = new BooleanProperty(false);
			Path path = Paths.get(uri);
			path.toFile().createNewFile();
			Stream<String> lines = Files.lines(path);
			List<String> replaced = lines
					.map(line -> {
						if (line.startsWith(key)) {
							line = newLine;
							alreadyExisted.setValue(true);
						}
						return line;
					})
					.collect(Collectors.toList());
			if (!alreadyExisted.getValue()) {
				replaced.add(newLine);
			}
			Files.write(path, replaced);
		});
	}
}
