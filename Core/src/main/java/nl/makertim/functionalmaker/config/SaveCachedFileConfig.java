package nl.makertim.functionalmaker.config;

import java.io.File;
import java.net.URI;

public class SaveCachedFileConfig extends CachedFileConfig {


	public SaveCachedFileConfig(File file) {
		super(file.toURI());
	}

	public SaveCachedFileConfig(String path) {
		super(new File(path));
	}

	public SaveCachedFileConfig(URI uri) {
		super(uri);
	}

	@Override
	protected void setInternally(String key, String rawValue) {
		super.setInternally(key, rawValue);
		save();
	}
}
