package nl.makertim.functionalmaker.config;

public interface SerializationFactory<T> {

	T serialize(String string);
}
