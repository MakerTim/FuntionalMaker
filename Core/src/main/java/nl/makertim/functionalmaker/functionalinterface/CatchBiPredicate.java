package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.BiPredicate;

@FunctionalInterface
public interface CatchBiPredicate<T, U, E extends Throwable> {

	boolean test(T t, U u) throws E;

	default CatchBiPredicate<T, U, E> and(BiPredicate<? super T, ? super U> other) {
		return (T t, U u) -> test(t, u) && other.test(t, u);
	}

	default CatchBiPredicate<T, U, E> and(CatchBiPredicate<? super T, ? super U, ? extends E> other) {
		return (T t, U u) -> test(t, u) && other.test(t, u);
	}

	default CatchBiPredicate<T, U, E> negate() {
		return (T t, U u) -> !test(t, u);
	}

	default CatchBiPredicate<T, U, E> or(BiPredicate<? super T, ? super U> other) {
		return (T t, U u) -> test(t, u) || other.test(t, u);
	}

	default CatchBiPredicate<T, U, E> or(CatchBiPredicate<? super T, ? super U, ? extends E> other) {
		return (T t, U u) -> test(t, u) || other.test(t, u);
	}

	static <T, U, E extends Throwable> CatchBiPredicate<T, U, E> convert(BiPredicate<T, U> predictate) {
		return predictate::test;
	}
}
