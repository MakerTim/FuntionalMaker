package nl.makertim.functionalmaker.functionalinterface;

import java.util.Comparator;

@FunctionalInterface
public interface CatchBinaryOperator<T, E extends Throwable> extends CatchBiFunction<T, T, T, E> {

	static <T, E extends Throwable> CatchBinaryOperator<T, E> minBy(CatchComparator<? super T, ? extends E> comparator) {
		return (a, b) -> comparator.compare(a, b) <= 0 ? a : b;
	}

	static <T, E extends Throwable> CatchBinaryOperator<T, E> minBy(Comparator<? super T> comparator) {
		return (a, b) -> comparator.compare(a, b) <= 0 ? a : b;
	}

	static <T, E extends Throwable> CatchBinaryOperator<T, E> maxBy(CatchComparator<? super T, ? extends E> comparator) {
		return (a, b) -> comparator.compare(a, b) >= 0 ? a : b;
	}

	static <T, E extends Throwable> CatchBinaryOperator<T, E> maxBy(Comparator<? super T> comparator) {
		return (a, b) -> comparator.compare(a, b) >= 0 ? a : b;
	}
}
