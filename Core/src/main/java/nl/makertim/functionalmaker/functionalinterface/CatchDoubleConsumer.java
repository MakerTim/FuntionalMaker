package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoubleConsumer;

@FunctionalInterface
public interface CatchDoubleConsumer<E extends Throwable> {

	void accept(double value) throws E;

	default CatchDoubleConsumer<E> andThen(CatchDoubleConsumer<? extends E> after) {
		return (double t) -> {
			accept(t);
			after.accept(t);
		};
	}

	default CatchDoubleConsumer<E> andThen(DoubleConsumer after) {
		return (double t) -> {
			accept(t);
			after.accept(t);
		};
	}

	static <E extends Throwable> CatchDoubleConsumer<E> convert(DoubleConsumer after) {
		return after::accept;
	}
}
