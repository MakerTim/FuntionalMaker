package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoubleSupplier;

@FunctionalInterface
public interface CatchDoubleSupplier<E extends Throwable> {

	double getAsDouble() throws E;

	static <E extends Throwable> CatchDoubleSupplier<E> convert(DoubleSupplier supplier) {
		return supplier::getAsDouble;
	}
}
