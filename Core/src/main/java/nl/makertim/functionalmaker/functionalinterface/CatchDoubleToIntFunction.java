package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoubleToIntFunction;

@FunctionalInterface
public interface CatchDoubleToIntFunction<E extends Throwable> {

	int applyAsInt(double value) throws E;

	static <E extends Throwable> CatchDoubleToIntFunction<E> convert(DoubleToIntFunction function) {
		return function::applyAsInt;
	}
}
