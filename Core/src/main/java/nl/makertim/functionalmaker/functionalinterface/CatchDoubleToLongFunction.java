package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoubleToLongFunction;

@FunctionalInterface
public interface CatchDoubleToLongFunction<E extends Throwable> {

	long applyAsLong(double value) throws E;

	static <E extends Throwable> CatchDoubleToLongFunction<E> convert(DoubleToLongFunction function) {
		return function::applyAsLong;
	}

}
