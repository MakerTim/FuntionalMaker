package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.DoubleUnaryOperator;

@FunctionalInterface
public interface CatchDoubleUnaryOperator<E extends Throwable> {

	double applyAsDouble(double operand) throws E;

	default CatchDoubleUnaryOperator<E> compose(CatchDoubleUnaryOperator<? extends E> before) {
		return (double v) -> applyAsDouble(before.applyAsDouble(v));
	}

	default CatchDoubleUnaryOperator<E> compose(DoubleUnaryOperator before) {
		return (double v) -> applyAsDouble(before.applyAsDouble(v));
	}

	default CatchDoubleUnaryOperator<E> andThen(CatchDoubleUnaryOperator<? extends E> after) {
		return (double t) -> after.applyAsDouble(applyAsDouble(t));
	}

	default CatchDoubleUnaryOperator<E> andThen(DoubleUnaryOperator after) {
		return (double t) -> after.applyAsDouble(applyAsDouble(t));
	}

	static <E extends Throwable> CatchDoubleUnaryOperator<E> identity() {
		return t -> t;
	}

	static <E extends Throwable> CatchDoubleUnaryOperator<E> convert(DoubleUnaryOperator operator) {
		return operator::applyAsDouble;
	}
}
