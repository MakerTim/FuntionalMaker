package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntBinaryOperator;

@FunctionalInterface
public interface CatchIntBinaryOperator<E extends Throwable> {

	int applyAsInt(int left, int right) throws E;

	static <E extends Throwable> CatchIntBinaryOperator<E> convert(IntBinaryOperator operator) {
		return operator::applyAsInt;
	}
}
