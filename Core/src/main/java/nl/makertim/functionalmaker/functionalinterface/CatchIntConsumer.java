package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntConsumer;

@FunctionalInterface
public interface CatchIntConsumer<E extends Throwable> {

	void accept(int value) throws E;

	default CatchIntConsumer<E> andThen(CatchIntConsumer<? extends E> after) {
		return (int t) -> {
			accept(t);
			after.accept(t);
		};
	}

	default CatchIntConsumer<E> andThen(IntConsumer after) {
		return (int t) -> {
			accept(t);
			after.accept(t);
		};
	}

	static <E extends Throwable> CatchIntConsumer<E> convert(IntConsumer consumer) {
		return consumer::accept;
	}
}
