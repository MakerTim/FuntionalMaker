package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.IntUnaryOperator;

@FunctionalInterface
public interface CatchIntUnaryOperator<E extends Throwable> {

	int applyAsInt(int operand) throws E;

	default CatchIntUnaryOperator<E> compose(CatchIntUnaryOperator<? extends E> before) {
		return (int v) -> applyAsInt(before.applyAsInt(v));
	}

	default CatchIntUnaryOperator<E> compose(IntUnaryOperator before) {
		return (int v) -> applyAsInt(before.applyAsInt(v));
	}

	default CatchIntUnaryOperator<E> andThen(CatchIntUnaryOperator<? extends E> after) {
		return (int t) -> after.applyAsInt(applyAsInt(t));
	}

	default CatchIntUnaryOperator<E> andThen(IntUnaryOperator after) {
		return (int t) -> after.applyAsInt(applyAsInt(t));
	}

	static <E extends Throwable> CatchIntUnaryOperator<E> identity() {
		return t -> t;
	}

	static <E extends Throwable> CatchIntUnaryOperator<E> convert(IntUnaryOperator operator) {
		return operator::applyAsInt;
	}
}
