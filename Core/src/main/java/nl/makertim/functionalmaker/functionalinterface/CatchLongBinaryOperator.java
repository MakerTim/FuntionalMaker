package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongBinaryOperator;

@FunctionalInterface
public interface CatchLongBinaryOperator<E extends Throwable> {

	long applyAsLong(long left, long right) throws E;

	static <E extends Throwable> CatchLongBinaryOperator<E> convert(LongBinaryOperator operator) {
		return operator::applyAsLong;
	}
}
