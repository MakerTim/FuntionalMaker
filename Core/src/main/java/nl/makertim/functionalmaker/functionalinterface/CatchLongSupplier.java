package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongSupplier;

@FunctionalInterface
public interface CatchLongSupplier<E extends Throwable> {

	long getAsLong() throws E;

	static <E extends Throwable> CatchLongSupplier<E> convert(LongSupplier supplier) {
		return supplier::getAsLong;
	}
}
