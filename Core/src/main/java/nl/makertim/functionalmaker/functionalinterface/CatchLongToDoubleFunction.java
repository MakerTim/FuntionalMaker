package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongToDoubleFunction;

@FunctionalInterface
public interface CatchLongToDoubleFunction<E extends Throwable> {

	double applyAsDouble(long value) throws E;

	static <E extends Throwable> CatchLongToDoubleFunction<E> convert(LongToDoubleFunction function) {
		return function::applyAsDouble;
	}
}
