package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.LongToIntFunction;

@FunctionalInterface
public interface CatchLongToIntFunction<E extends Throwable> {

	int applyAsInt(long value) throws E;

	static <E extends Throwable> CatchLongToIntFunction<E> convert(LongToIntFunction function) {
		return function::applyAsInt;
	}
}
