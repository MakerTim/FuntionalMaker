package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ObjIntConsumer;

@FunctionalInterface
public interface CatchObjIntConsumer<T, E extends Throwable> {

	void accept(T t, int value) throws E;

	static <T, E extends Throwable> CatchObjIntConsumer<T, E> convert(ObjIntConsumer<T> consumer) {
		return consumer::accept;
	}
}
