package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchQuadFunction<T, U, V, W, X, E extends Throwable> {

	X apply(T t, U u, V v, W w) throws E;

	static <T, U, V, W, X, E extends Throwable> CatchQuadFunction<T, U, V, W, X, E> convert(QuadFunction<T, U, V, W, X> function) {
		return function::apply;
	}
}
