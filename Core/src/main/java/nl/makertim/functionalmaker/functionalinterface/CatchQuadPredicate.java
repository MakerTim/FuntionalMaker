package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchQuadPredicate<T, U, V, W, E extends Throwable> {

	boolean test(T t, U u, V v, W w) throws E;

	default CatchQuadPredicate<T, U, V, W, E> and(QuadPredicate<? super T, ? super U, ? super V, ? super W> other) {
		return (T t, U u, V v, W w) -> test(t, u, v, w) && other.test(t, u, v, w);
	}

	default CatchQuadPredicate<T, U, V, W, E> and(CatchQuadPredicate<? super T, ? super U, ? super V, ? super W, ? extends E> other) {
		return (T t, U u, V v, W w) -> test(t, u, v, w) && other.test(t, u, v, w);
	}

	default CatchQuadPredicate<T, U, V, W, E> negate() {
		return (T t, U u, V v, W w) -> !test(t, u, v, w);
	}

	default CatchQuadPredicate<T, U, V, W, E> or(QuadPredicate<? super T, ? super U, ? super V, ? super W> other) {
		return (T t, U u, V v, W w) -> test(t, u, v, w) || other.test(t, u, v, w);
	}

	default CatchQuadPredicate<T, U, V, W, E> or(CatchQuadPredicate<? super T, ? super U, ? super V, ? super W, ? extends E> other) {
		return (T t, U u, V v, W w) -> test(t, u, v, w) || other.test(t, u, v, w);
	}

	static <T, U, V, W, E extends Throwable> CatchQuadPredicate<T, U, V, W, E> convert(QuadPredicate<T, U, V, W> predicate) {
		return predicate::test;
	}
}
