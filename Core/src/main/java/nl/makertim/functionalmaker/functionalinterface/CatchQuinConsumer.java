package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchQuinConsumer<T, U, V, W, X, E extends Throwable> {

	void accept(T t, U u, V v, W w, X x) throws E;

	default CatchQuinConsumer<T, U, V, W, X, E> andThen(CatchQuinConsumer<? super T, ? super U, ? super V, ? super W, ? super X, ? extends E> after) {
		return (l, r, v, w, x) -> {
			accept(l, r, v, w, x);
			after.accept(l, r, v, w, x);
		};
	}

	default CatchQuinConsumer<T, U, V, W, X, E> before(CatchQuinConsumer<? super T, ? super U, ? super V, ? super W, ? super X, ? extends E> before) {
		return (l, r, v, w, x) -> {
			before.accept(l, r, v, w, x);
			accept(l, r, v, w, x);
		};
	}

	static <T, U, V, W, X, E extends Throwable> CatchQuinConsumer<T, U, V, W, X, E> convert(QuinConsumer<T, U, V, W, X> consumer) {
		return consumer::accept;
	}
}
