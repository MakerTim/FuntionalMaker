package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.Supplier;

@FunctionalInterface
public interface CatchSupplier<T, E extends Throwable> {

	T get() throws E;

	static <T, E extends Throwable> CatchSupplier<T, E> convert(Supplier<T> supplier) {
		return supplier::get;
	}
}
