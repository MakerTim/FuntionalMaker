package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ToDoubleBiFunction;

@FunctionalInterface
public interface CatchToDoubleBiFunction<T, U, E extends Throwable> {

	double applyAsDouble(T t, U u) throws E;

	static <T, U, E extends Throwable> CatchToDoubleBiFunction<T, U, E> convert(ToDoubleBiFunction<T, U> function) {
		return function::applyAsDouble;
	}
}
