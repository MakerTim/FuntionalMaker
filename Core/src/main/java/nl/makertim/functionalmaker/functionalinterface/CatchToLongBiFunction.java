package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ToLongBiFunction;

@FunctionalInterface
public interface CatchToLongBiFunction<T, U, E extends Throwable> {

	long applyAsLong(T t, U u) throws E;

	static <T, U, E extends Throwable> CatchToLongBiFunction<T, U, E> convert(ToLongBiFunction<T, U> function) {
		return function::applyAsLong;
	}
}
