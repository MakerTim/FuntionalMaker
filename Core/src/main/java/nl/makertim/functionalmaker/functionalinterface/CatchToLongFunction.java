package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.ToLongFunction;

@FunctionalInterface
public interface CatchToLongFunction<T, E extends Throwable> {

	long applyAsLong(T t) throws E;

	static <T, E extends Throwable> CatchToLongFunction<T, E> convert(ToLongFunction<T> function) {
		return function::applyAsLong;
	}
}
