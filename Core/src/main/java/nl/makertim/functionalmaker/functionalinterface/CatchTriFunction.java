package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchTriFunction<T, U, V, W, E extends Throwable> {

	W apply(T t, U u, V v) throws E;

	static <T, U, V, W, E extends Throwable> CatchTriFunction<T, U, V, W, E> convert(TriFunction<T, U, V, W> function) {
		return function::apply;
	}

}
