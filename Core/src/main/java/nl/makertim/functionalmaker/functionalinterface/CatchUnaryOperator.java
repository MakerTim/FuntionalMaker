package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface CatchUnaryOperator<T, E extends Throwable> extends CatchFunction<T, T, E> {

	static <T, E extends Throwable> CatchUnaryOperator<T, E> identity() {
		return t -> t;
	}
}
