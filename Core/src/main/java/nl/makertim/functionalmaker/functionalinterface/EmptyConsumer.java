package nl.makertim.functionalmaker.functionalinterface;

import java.util.function.Consumer;

public class EmptyConsumer<T> implements Consumer<T> {

	@Override
	public void accept(T t) {
		// does nothing
	}
}
