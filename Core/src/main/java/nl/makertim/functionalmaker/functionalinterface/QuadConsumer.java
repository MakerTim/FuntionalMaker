package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface QuadConsumer<T, U, V, W> {

	void accept(T t, U u, V v, W w);

	default QuadConsumer<T, U, V, W> andThen(QuadConsumer<? super T, ? super U, ? super V, ? super W> after) {
		return (l, r, v, w) -> {
			accept(l, r, v, w);
			after.accept(l, r, v, w);
		};
	}

	default QuadConsumer<T, U, V, W> before(QuadConsumer<? super T, ? super U, ? super V, ? super W> before) {
		return (l, r, v, w) -> {
			before.accept(l, r, v, w);
			accept(l, r, v, w);
		};
	}
}
