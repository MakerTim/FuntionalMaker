package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface QuadFunction<T, U, V, W, X> {

	X apply(T t, U u, V v, W w);

}
