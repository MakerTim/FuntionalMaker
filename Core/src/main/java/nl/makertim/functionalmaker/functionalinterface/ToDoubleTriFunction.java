package nl.makertim.functionalmaker.functionalinterface;

@FunctionalInterface
public interface ToDoubleTriFunction<T, U, V> {

    double applyAsDouble(T t, U u, V v);
}
