package nl.makertim.functionalmaker.optional;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class UpdateAbleOptional<T> {

	private T value;

	private UpdateAbleOptional() {
		this.value = null;
	}

	public static <T> UpdateAbleOptional<T> empty() {
		return new UpdateAbleOptional<>();
	}

	private UpdateAbleOptional(T value) {
		this.value = Objects.requireNonNull(value);
	}


	public static <T> UpdateAbleOptional<T> of(T value) {
		return new UpdateAbleOptional<>(value);
	}

	public static <T> UpdateAbleOptional<T> ofNullable(T value) {
		return value == null ? empty() : of(value);
	}

	public T get() {
		if (value == null) {
			throw new NoSuchElementException("No value present");
		}
		return value;
	}

	public boolean isPresent() {
		return value != null;
	}

	public void ifPresent(Consumer<? super T> action) {
		if (value != null) {
			action.accept(value);
		}
	}

	public void ifPresentOrElse(Consumer<? super T> action, Runnable emptyAction) {
		if (value != null) {
			action.accept(value);
		} else {
			emptyAction.run();
		}
	}

	public UpdateAbleOptional<T> filter(Predicate<? super T> predicate) {
		Objects.requireNonNull(predicate);
		if (!isPresent()) {
			return this;
		} else {
			return predicate.test(value) ? this : empty();
		}
	}

	public <U> UpdateAbleOptional<U> map(Function<? super T, ? extends U> mapper) {
		Objects.requireNonNull(mapper);
		if (!isPresent()) {
			return empty();
		} else {
			return UpdateAbleOptional.ofNullable(mapper.apply(value));
		}
	}

	public <U> UpdateAbleOptional<U> flatMap(Function<? super T, ? extends UpdateAbleOptional<? extends U>> mapper) {
		Objects.requireNonNull(mapper);
		if (!isPresent()) {
			return empty();
		} else {
			@SuppressWarnings("unchecked")
			UpdateAbleOptional<U> r = (UpdateAbleOptional<U>) mapper.apply(value);
			return Objects.requireNonNull(r);
		}
	}

	public UpdateAbleOptional<T> or(Supplier<? extends UpdateAbleOptional<? extends T>> supplier) {
		Objects.requireNonNull(supplier);
		if (isPresent()) {
			return this;
		} else {
			@SuppressWarnings("unchecked")
			UpdateAbleOptional<T> r = (UpdateAbleOptional<T>) supplier.get();
			return Objects.requireNonNull(r);
		}
	}

	public Stream<T> stream() {
		if (!isPresent()) {
			return Stream.empty();
		} else {
			return Stream.of(value);
		}
	}

	public T orElse(T other) {
		return value != null ? value : other;
	}

	public T orElseUpdate(T other) {
		return value != null ? value : updateInternal(other);
	}

	public T orElseGet(Supplier<? extends T> supplier) {
		return value != null ? value : supplier.get();
	}

	public T orElseGetUpdate(Supplier<? extends T> supplier) {
		return value != null ? value : updateInternal(supplier.get());
	}

	protected T updateInternal(T other) {
		if (other != null) {
			value = other;
		}
		return other;
	}

	public void update(T other) {
		updateInternal(other);
	}

	public T orElseThrow() {
		return get();
	}

	public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
		if (value != null) {
			return value;
		} else {
			throw exceptionSupplier.get();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UpdateAbleOptional)) {
			return false;
		}

		UpdateAbleOptional<?> other = (UpdateAbleOptional<?>) obj;
		return Objects.equals(value, other.value);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(value);
	}

	@Override
	public String toString() {
		return value != null
				? String.format("UpdateAbleOptional[%s]", value)
				: "UpdateAbleOptional.empty";
	}
}
