package nl.makertim.functionalmaker.property;

import java.util.function.Supplier;

public class ByteProperty implements Supplier<Byte> {

	protected byte value;

	public ByteProperty(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public void addOne() {
		value++;
	}

	public void removeOne() {
		value--;
	}

	public void setValue(byte value) {
		this.value = value;
	}

	@Override
	public Byte get() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}
}
