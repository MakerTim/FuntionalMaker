package nl.makertim.functionalmaker.property;

import java.util.function.DoubleSupplier;

public class DoubleProperty implements DoubleSupplier {

	protected double value;

	public DoubleProperty(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void addOne() {
		value++;
	}

	public void removeOne() {
		value--;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public double getAsDouble() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(getAsDouble());
	}
}
