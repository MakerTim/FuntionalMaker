package nl.makertim.functionalmaker.property;

import java.util.function.Supplier;

public class FloatProperty implements Supplier<Float> {

	protected float value;

	public FloatProperty(float value) {
		this.value = value;
	}

	public float getValue() {
		return value;
	}

	public void addOne() {
		value++;
	}

	public void removeOne() {
		value--;
	}

	public void setValue(float value) {
		this.value = value;
	}

	@Override
	public Float get() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}
}
