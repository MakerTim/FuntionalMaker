package nl.makertim.functionalmaker.property;

import java.util.function.IntSupplier;

public class IntProperty implements IntSupplier {

	protected int value;

	public IntProperty(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void addOne() {
		value++;
	}

	public void removeOne() {
		value--;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public int getAsInt() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(getValue());
	}
}
