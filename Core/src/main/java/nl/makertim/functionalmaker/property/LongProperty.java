package nl.makertim.functionalmaker.property;

import java.util.function.LongSupplier;

public class LongProperty implements LongSupplier {

	protected long value;

	public LongProperty(long value) {
		this.value = value;
	}

	public long getValue() {
		return value;
	}

	public void addOne() {
		value++;
	}

	public void removeOne() {
		value--;
	}

	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public long getAsLong() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(getAsLong());
	}
}
