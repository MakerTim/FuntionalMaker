package nl.makertim.functionalmaker.property;

import java.util.function.Supplier;

public class ShortProperty implements Supplier<Short> {

	protected short value;

	public ShortProperty(short value) {
		this.value = value;
	}

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	@Override
	public Short get() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}
}
