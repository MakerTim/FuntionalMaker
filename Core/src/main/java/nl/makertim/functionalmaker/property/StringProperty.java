package nl.makertim.functionalmaker.property;

import java.util.function.Supplier;

public class StringProperty implements Supplier<String> {

	protected String value;

	public StringProperty(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public int length() {
		return value.length();
	}

	public void append(String string) {
		value += string;
	}

	public void append(char c) {
		value += c;
	}

	public void append(Object obj) {
		value += obj.toString();
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isNotNull() {
		return value != null;
	}

	public boolean isNull() {
		return value == null;
	}

	@Override
	public String get() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}
}
