package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.threading.Sync;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.*;

public class SyncTest {

	private int anInt;
	private IntSupplier supplierI = () -> anInt;
	private long aLong;
	private LongSupplier supplierL = () -> aLong;
	private double aDouble;
	private DoubleSupplier supplierD = () -> aDouble;
	private Object object;
	private Supplier<Object> supplierO = () -> object;
	private boolean aBoolean;
	private BooleanSupplier supplierB = () -> aBoolean;

	@Before
	public void reset() {
		anInt = 0;
		aLong = 0;
		aDouble = 0;
		object = null;
		aBoolean = false;
	}

	@Test
	public void waitForIntBetween() {
		int expected = 100; // Number between 0 & 150

		anInt = 1000;
		delayed(() -> anInt = 500, 50);
		delayed(() -> anInt = 250, 100);
		delayed(() -> anInt = expected, 150);
		Sync.waitFor(supplierI, 0, 150);

		Assert.assertEquals(75, anInt, 75);
	}

	@Test
	public void waitForIntExact() {
		int expected = 0;

		anInt = 100;
		delayed(() -> anInt = 50, 50);
		delayed(() -> anInt = 25, 100);
		delayed(() -> anInt = expected, 150);
		Sync.waitFor(supplierI, expected);

		Assert.assertEquals(expected, anInt);
	}

	@Test
	public void waitForLongBetween() {
		long expected = Integer.MAX_VALUE * 6L;

		aLong = Integer.MAX_VALUE * 2L;
		delayed(() -> aLong = Integer.MAX_VALUE * 3L, 50);
		delayed(() -> aLong = Integer.MAX_VALUE * 4L, 100);
		delayed(() -> aLong = Integer.MAX_VALUE * 5L, 150);
		delayed(() -> aLong = expected, 200);
		Sync.waitFor(supplierL, expected, expected + 1L);

		Assert.assertEquals(expected, aLong);
	}

	@Test
	public void waitForLongExact() {
		long expected = -103302;

		aLong = 123456789;
		delayed(() -> aLong = 987654321, 50);
		delayed(() -> aLong = 15, 100);
		delayed(() -> aLong = 73, 150);
		delayed(() -> aLong = 34, 200);
		delayed(() -> aLong = 1234567890, 250);
		delayed(() -> aLong = expected, 300);
		Sync.waitFor(supplierL, expected);

		Assert.assertEquals(expected, aLong);
	}

	@Test
	public void waitForDoubleBetween() {
		double expected = 800;

		aDouble = 0;
		delayed(() -> aDouble = Math.sqrt(2), 50);
		delayed(() -> aDouble = 1.414213D, 100);
		delayed(() -> aDouble = -10, 150);
		delayed(() -> aDouble = Math.E, 200);
		delayed(() -> aDouble = Math.PI, 250);
		delayed(() -> aDouble = expected, 300);
		Sync.waitFor(supplierD, 700, 900);

		Assert.assertEquals(800, aDouble, 100D);
	}

	@Test
	public void waitForDoubleExact() {
		double expected = Math.PI;

		aDouble = 0;
		delayed(() -> aDouble = 404, 50);
		delayed(() -> aDouble = 1.414213D, 100);
		delayed(() -> aDouble = -10, 150);
		delayed(() -> aDouble = Math.E, 200);
		delayed(() -> aDouble = Math.PI * 2, 250);
		delayed(() -> aDouble = expected, 300);
		Sync.waitFor(supplierD, expected);

		Assert.assertEquals(expected, aDouble, 0D);
	}

	@Test
	public void waitForObjectExact() {
		Object expected = new Object();

		delayed(() -> object = 1, 50);
		delayed(() -> object = -1D, 100);
		delayed(() -> object = null, 150);
		delayed(() -> object = expected, 200);
		Sync.waitFor(supplierO, expected);

		Assert.assertEquals(expected, object);
	}

	@Test
	public void waitForCollection() {
		List<Object> list = new ArrayList<>();

		delayed(() -> list.add(null), 25);
		delayed(() -> list.add(null), 50);
		delayed(() -> list.add(null), 75);
		delayed(() -> list.add(null), 100);
		Sync.waitForAdditionCollection(list);

		Assert.assertEquals(4, list.size());
	}

	@Test
	public void waitForBoolean() {
		boolean expected = true;

		delayed(() -> aBoolean = true, 100);
		Sync.waitFor(supplierB);

		Assert.assertEquals(expected, aBoolean);
	}

	private void delayed(Runnable doThis, long time) {
		Try.execute(() -> Thread.sleep(time)).then(doThis);
	}
}
