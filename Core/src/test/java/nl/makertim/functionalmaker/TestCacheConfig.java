package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.config.CacheConfig;
import nl.makertim.functionalmaker.config.Config;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;
import java.util.UUID;

public class TestCacheConfig {

	@Test
	public void keys() {
		Config config = new CacheConfig();
		config.clear();

		config.setString("A", "READY");
		config.setString("A", "SET");
		config.setString("A", "GO");
		config.setString("B", "GO");
		config.setString("C", "GO");
		config.setString("D", "GO");

		Assert.assertArrayEquals(new String[]{"A", "B", "C", "D"}, config.getKeys());
	}

	@Test
	public void testEmpty() {
		Config config = new CacheConfig();

		config.setString("A", "READY");

		Assert.assertNull(config.getString("NOT-SET"));
	}

	@Test
	public void wrongType() {
		Config config = new CacheConfig();

		config.setString("A", "READY");

		Assert.assertNull(config.getUUID("A"));
	}


	@Test
	public void customType() {
		Config config = new CacheConfig();

		config.setObject("A", Optional.of("TEST"), type -> type.orElse(""));

		Assert.assertEquals(Optional.of("TEST"), config.getObject("A", raw -> Optional.ofNullable((raw.isEmpty() ? null : raw))));
	}

	@Test
	public void missingCustomType() {
		Config config = new CacheConfig();

		config.setObject("A", Optional.of("TEST"), type -> type.orElse(""));

		Assert.assertNull(config.getObject("NON-EXISTING", raw -> Optional.ofNullable((raw.isEmpty() ? null : raw))));
	}

	@Test
	public void wrongCustomType() {
		Config config = new CacheConfig();

		config.setString("A", "a");

		Assert.assertNull(config.getObject("A", raw -> Optional.ofNullable((raw.isEmpty() ? null : raw))));
	}

	@Test
	public void setOverwrite() {
		Config config = new CacheConfig();

		config.setString("A", "READY");
		config.setString("A", "SET");
		config.setString("A", "GO");

		Assert.assertEquals("GO", config.getString("A"));
	}

	@Test
	public void setString() {
		Config config = new CacheConfig();

		config.setString("A", "ABC");
		config.setString("B", "tim");

		Assert.assertEquals("ABC", config.getString("A"));
		Assert.assertEquals("tim", config.getString("B"));
	}

	@Test
	public void setDouble() {
		Config config = new CacheConfig();

		config.setDouble("MAX", Double.MAX_VALUE);
		config.setDouble("MIN", Double.MIN_VALUE);
		config.setDouble("INF", Double.POSITIVE_INFINITY);

		Assert.assertEquals(Double.MAX_VALUE, config.getDouble("MAX"), 0);
		Assert.assertEquals(Double.MIN_VALUE, config.getDouble("MIN"), 0);
		Assert.assertEquals(Double.POSITIVE_INFINITY, config.getDouble("INF"), 0);
	}

	@Test
	public void setLong() {
		Config config = new CacheConfig();

		config.setLong("MAX", Long.MAX_VALUE);
		config.setLong("MIN", Long.MIN_VALUE);

		Assert.assertEquals(Long.MAX_VALUE, config.getLong("MAX"), 0);
		Assert.assertEquals(Long.MIN_VALUE, config.getLong("MIN"), 0);
	}

	@Test
	public void setInt() {
		Config config = new CacheConfig();

		config.setInt("MAX", Integer.MAX_VALUE);
		config.setInt("MIN", Integer.MIN_VALUE);

		Assert.assertEquals(Integer.MAX_VALUE, config.getInt("MAX"), 0);
		Assert.assertEquals(Integer.MIN_VALUE, config.getInt("MIN"), 0);
	}

	@Test
	public void setUUID() {
		Config config = new CacheConfig();

		UUID ra = UUID.randomUUID();
		UUID ra2 = UUID.randomUUID();

		config.setUUID("RA", ra);
		config.setUUID("RA2", ra2);

		Assert.assertEquals(ra, config.getUUID("RA"));
		Assert.assertEquals(ra2, config.getUUID("RA2"));
	}
}
