package nl.makertim.functionalmaker;

import nl.makertim.functionalmaker.property.IntProperty;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class TryTest {

	private final Logger logger = Logger.getGlobal();

	@Test
	public void withoutData() {
		Try.execute(() -> {
			IntProperty zero = new IntProperty(0);
			int lengthPer = 100 / zero.getValue();
			logger.warning(Integer.toString(lengthPer));
		}).then(() -> {
			throw new AssertionError("There should be an error ahead, without data!");
		}).orElse(() -> {
			logger.warning("Yay it works without data!");
		}).always(() -> {
			logger.warning("And as always, thanks for watching nothing");
		});

		Try.execute(() -> {
		}).orThrow(RuntimeException::new);
	}

	@Test
	public void withData() {
		Try.execute(() -> {
			String helloWorld = "Hello";
			return 100 / helloWorld.length();
		}).then(() -> {
			logger.warning("Yay it works with data!");
		}).then(per -> {
			logger.warning("It is " + per);
		}).orElse(() -> {
			throw new AssertionError("There shouldn't be an error ahead!");
		}).always(() -> {
			logger.warning("And as always, now with content thanks for watching");
		}).orThrow(RuntimeException::new);
	}

	@Test
	public void withoutDataAsync() {
		Try.executeAsync(() -> {
					Thread.sleep(1000L);
					IntProperty intProperty = new IntProperty(0);
					int lengthPer = 100 / intProperty.getValue();
					logger.warning(Integer.toString(lengthPer));
				}, Executors.newCachedThreadPool(),
				result -> result.then(() -> {
					throw new AssertionError("There shouldn't be an error ahead!");
				}).orElse(() -> {
					logger.warning("Yay it works!");
				}).always(() -> {
					logger.warning("And as always, thanks for watching");
				}));
	}

	@Test
	public void withDataAsync() {
		Try.executeAsync(() -> {
					Thread.sleep(1000L);
					IntProperty intProperty = new IntProperty(0);
					return 100 / intProperty.getValue();
				}, Executors.newCachedThreadPool(),
				result -> result.then(() -> {
					throw new AssertionError("There sould be an error ahead!");
				}).orElse(() -> {
					logger.warning("Yay it works!");
				}).always(() -> {
					logger.warning("And as always, thanks for watching");
				}));
	}

	@Test
	public void executeRunWithError() {
		IntProperty count = new IntProperty(0);
		Try.Tried tried = Try.execute(() -> {
			count.addOne();
			if ("0".equals(Integer.toString(0)))
				throw new AssertionError();
		}).onFail(thr -> count.addOne());

		Assert.assertSame(2, count.getValue());


		Try.execute(() -> {
			tried.orThrow(RuntimeException::new);
		}).orElse(count::addOne);

		Assert.assertSame(3, count.getValue());
	}

	@Test
	public void executeSupplierWithError() {
		IntProperty count = new IntProperty(0);

		Try.Tried tried = Try.execute(() -> {
			count.addOne();
			if ("0".equals(Integer.toString(0)))
				throw new AssertionError();
			return new Object();
		}).onFail(thr -> {
			count.addOne();
		});
		Try.execute(() -> {
			tried.orThrow(RuntimeException::new);
		}).orElse(count::addOne);

		Assert.assertSame(3, count.getValue());


		IntProperty count2 = new IntProperty(0);

		int data = Try.execute(() -> {
			if ("0".equals(Integer.toString(0)))
				throw new AssertionError();
			return 1;
		}).onFail(thr -> {
			count2.addOne();
		}).orElse(() -> 2
		).orElse(x -> 3
		).orElse(count2::addOne).getData();

		Assert.assertSame(2, count2.getValue());
		Assert.assertSame(3, data);


		Try.execute(() -> {
			tried.orThrow(RuntimeException::new);
		}).orElse(count2::addOne);

		Assert.assertSame(3, count2.getValue());
	}

}
