package nl.makertim.functionalmaker.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class MethodFinder {

	private MethodFinder() {
	}

	public static Collection<Method> getMethodsOfClass(Class cls) {
		return getMethodsOfClass(cls, true);
	}

	public static Collection<Method> getMethodsOfClass(Class cls, boolean includeSuperclassMethods) {
		Collection<Method> methods = new LinkedHashSet<>(Arrays.asList(cls.getDeclaredMethods()));

		if (cls.getSuperclass() != null && includeSuperclassMethods) {
			methods.addAll(getMethodsOfClass(cls.getSuperclass(), true));
		}

		return methods;
	}

	public static Collection<Method> getMethodsWithAnnotationOfClass(Class cls, Class<? extends Annotation> annotation) {
		return getMethodsWithAnnotationOfClass(cls, annotation, true);
	}

	public static Collection<Method> getMethodsWithAnnotationOfClass(Class cls, Class<? extends Annotation> annotation, boolean includeSuperclassMethods) {
		Collection<Method> methods = getMethodsOfClass(cls, includeSuperclassMethods);

		return methods.stream() //
				.filter(method -> AnnotationFinder.hasAnnotation(method.getAnnotations(), annotation))
				.collect(Collectors.toList());
	}
}
