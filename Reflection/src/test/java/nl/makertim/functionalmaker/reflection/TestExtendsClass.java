package nl.makertim.functionalmaker.reflection;

public class TestExtendsClass extends TestClass {

	@TestAnnotation
	private String a;
	@TestAnnotation
	private String b;
	@TestAnnotation
	private String c;

	@TestAnnotation
	private void s() {
	}

	@TestAnnotation
	private void t() {
	}

	@TestAnnotation
	private void v() {
	}
}
