package nl.makertim.sql.core.command;

public interface Command<S extends Command<S>> {

	CommandType<S> getType();

	String getTable();

}
