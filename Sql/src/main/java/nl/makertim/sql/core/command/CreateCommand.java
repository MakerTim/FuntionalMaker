package nl.makertim.sql.core.command;

import nl.makertim.sql.core.types.Column;

import java.util.*;

public class CreateCommand extends AbstractCommand<CreateCommand> {

	protected LinkedList<Column> columns;
	protected boolean ignoreIfExists = false;

	public CreateCommand(String table, List<Column> columns) {
		super(table);
		this.columns = new LinkedList<>(columns);
	}

	public CreateCommand(String table, Column... columns) {
		this(table, Arrays.asList(columns));
	}

	public void setIgnoreIfExists(boolean ignore) {
		this.ignoreIfExists = ignore;
	}

	public boolean shouldIgnoreIfExists() {
		return ignoreIfExists;
	}

	@Override
	public CommandType<CreateCommand> getType() {
		return CommandType.CREATE;
	}

	public void addColumn(Column column) {
		columns.add(column);
	}

	public void addColumnAt(int index, Column column) {
		columns.add(index, column);
	}

	public void addColumnBefore(Column columnToBefore, Column column) {
		addColumnBefore(columnToBefore.columnName(), column);
	}

	public void addColumnBefore(String columnName, Column column) {
		addColumnOffset(columnName, column, 0);
	}

	public void addColumnAfter(Column columnToAfter, Column column) {
		addColumnAfter(columnToAfter.columnName(), column);
	}

	public void addColumnAfter(String columnName, Column column) {
		addColumnOffset(columnName, column, 1);
	}

	private void addColumnOffset(String columnName, Column column, int offset) {
		Optional<Column> existing = columns.stream().filter(existingColumn -> existingColumn.columnName().equalsIgnoreCase(columnName)).findFirst();
		if (existing.isPresent()) {
			int index = columns.indexOf(existing.get());
			columns.add(index + offset, column);
		} else {
			columns.addLast(column);
		}
	}

	public List<Column> getColumns() {
		return Collections.unmodifiableList(columns);
	}

}
