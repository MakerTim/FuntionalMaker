package nl.makertim.sql.core.command;

public interface LimitedCommand<S extends Command<S>> {

	// -1 = unlimited
	int getRows();

	void setRows(int rows);
}
