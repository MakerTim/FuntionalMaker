package nl.makertim.sql.core.command;

public interface ParameterCommand<S extends Command<S>> {

	void addParameter(Object value);

	Object[] getParameters();

}
