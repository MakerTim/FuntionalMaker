package nl.makertim.sql.core.command;

import nl.makertim.sql.core.order.Order;
import nl.makertim.sql.core.where.Where;

import java.util.ArrayList;
import java.util.List;

public class SelectCommand extends AbstractCommand<SelectCommand>
		implements ColumnsSelectableCommand<SelectCommand>, WhereCommand<SelectCommand>, OrderedCommand<SelectCommand>, OffsetCommand<SelectCommand> {

	private boolean hasDupes = true;
	private String[] columns;
	private Where where;
	private List<Object> variables = new ArrayList<>();
	private Order order = new Order();
	private int rows = -1;
	private int start = -1;

	public SelectCommand(String table, String[] columns, Where where, Order order, int rows) {
		this(table, columns, where, order);
		this.rows = rows;
	}

	public SelectCommand(String table, String[] columns, Where where, Order order) {
		this(table, columns, where);
		this.order = order;
	}

	public SelectCommand(String table, String[] columns, Where where) {
		super(table);
		this.columns = columns;
		this.where = where;
	}

	public SelectCommand(String table, String[] columns) {
		this(table, columns, Where.NONE);
	}

	public SelectCommand(String table, Where where) {
		this(table, null, where);
	}

	public SelectCommand(String table) {
		this(table, Where.NONE);
	}

	public SelectCommand hasDupes(boolean hasDupes) {
		this.hasDupes = hasDupes;
		return this;
	}

	public boolean hasDupes() {
		return hasDupes;
	}

	@Override
	public String[] getColumns() {
		return columns;
	}

	@Override
	public CommandType<SelectCommand> getType() {
		return CommandType.SELECT;
	}

	@Override
	public Where getWhere() {
		return where;
	}

	@Override
	public void addParameter(Object value) {
		variables.add(value);
	}

	@Override
	public Object[] getParameters() {
		return variables.toArray(new Object[0]);
	}

	@Override
	public Order getOrder() {
		return order;
	}

	@Override
	public int getStart() {
		return start;
	}

	@Override
	public int getRows() {
		return rows;
	}

	@Override
	public void setStart(int start) {
		this.start = start;
	}

	@Override
	public void setRows(int rows) {
		this.rows = rows;
	}

	@Override
	public void setStartAndRows(int start, int rows) {
		setStart(start);
		setRows(rows);
	}
}
