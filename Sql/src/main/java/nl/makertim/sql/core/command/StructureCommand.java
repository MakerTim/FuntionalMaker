package nl.makertim.sql.core.command;

public class StructureCommand extends AbstractCommand<StructureCommand> {

	public StructureCommand(String table) {
		super(table);
	}

	@Override
	public CommandType<StructureCommand> getType() {
		return CommandType.GET_STRUCTURE;
	}
}
