package nl.makertim.sql.core.command;

public class TruncateCommand extends AbstractCommand<TruncateCommand> {

	public TruncateCommand(String table) {
		super(table);
	}

	@Override
	public CommandType<TruncateCommand> getType() {
		return CommandType.TRUNCATE;
	}
}
