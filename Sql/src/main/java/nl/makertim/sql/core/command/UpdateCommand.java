package nl.makertim.sql.core.command;

import nl.makertim.sql.core.order.Order;
import nl.makertim.sql.core.where.Where;

import java.util.*;

public class UpdateCommand extends AbstractCommand<UpdateCommand>
		implements WhereCommand<UpdateCommand>, RowCommand<UpdateCommand>, OrderedCommand<UpdateCommand>, LimitedCommand<UpdateCommand> {

	private Map<String, Object> setters = new LinkedHashMap<>();
	private Where where;
	private List<Object> variables = new ArrayList<>();
	private Order order = new Order();
	private int rows = -1;

	public UpdateCommand(String table, String column, String value) {
		super(table);
		addValue(column, value);
	}

	public UpdateCommand(String table, String column, String value, int limit) {
		this(table, column, value);
		this.rows = limit;
	}

	public UpdateCommand(String table, String column, String value, Where where) {
		this(table, column, value);
		this.where = where;
	}

	public UpdateCommand(String table, String column, String value, Where where, int limit) {
		this(table, column, value, where);
		this.rows = limit;
	}

	public UpdateCommand(String table, String column, String value, Where where, Order order) {
		this(table, column, value, where);
		this.order = order;
	}

	public UpdateCommand(String table, String column, String value, Where where, Order order, int limit) {
		this(table, column, value, where, limit);
		this.order = order;
	}

	@Override
	public CommandType<UpdateCommand> getType() {
		return CommandType.UPDATE;
	}

	@Override
	public void addValue(String column, Object value) {
		setters.put(column, value);
	}

	@Override
	public Map<String, Object> getValues() {
		return Collections.unmodifiableMap(setters);
	}

	@Override
	public Where getWhere() {
		return where;
	}

	@Override
	public void addParameter(Object value) {
		variables.add(value);
	}

	@Override
	public Object[] getParameters() {
		return variables.toArray(new Object[0]);
	}

	@Override
	public int getRows() {
		return rows;
	}

	@Override
	public void setRows(int rows) {
		this.rows = rows;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public Order getOrder() {
		return order;
	}
}
