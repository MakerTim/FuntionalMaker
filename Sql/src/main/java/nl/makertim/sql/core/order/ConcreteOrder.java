package nl.makertim.sql.core.order;

public class ConcreteOrder {

	private String column;
	private OrderType type;

	public ConcreteOrder(String column, OrderType type) {
		this.column = column;
		this.type = type;
	}

	public String getColumn() {
		return column;
	}

	public OrderType getType() {
		return type;
	}
}
