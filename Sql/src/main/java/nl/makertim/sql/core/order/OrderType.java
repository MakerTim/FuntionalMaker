package nl.makertim.sql.core.order;

public enum OrderType {
	ASCENDING,
	DESCENDING
}
