package nl.makertim.sql.core.translator;

import nl.makertim.functionalmaker.Try;
import nl.makertim.sql.core.command.*;
import nl.makertim.sql.core.order.Order;
import nl.makertim.sql.core.order.OrderType;
import nl.makertim.sql.core.types.Column;
import nl.makertim.sql.core.types.ColumnFactory;
import nl.makertim.sql.core.types.Type;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public interface CommandTranslator {

	char getQuote();

	String escapeQuote();

	char stringQuote();

	String escapeStringQuote();

	char separator();

	WhereTranslator getWhereTranslator();

	TypeTranslator getTypeTranslator();

	default String orderName(OrderType type) {
		switch (type) {
		case ASCENDING:
			return "ASC";
		default:
		case DESCENDING:
			return "DESC";
		}
	}

	default String safeName(String table) {
		return getQuote() + table.replaceAll(Character.toString(getQuote()), escapeQuote()) + getQuote();
	}

	default String safeValue(String table) {
		return stringQuote() + table.replaceAll(Character.toString(stringQuote()), escapeStringQuote()) + stringQuote();
	}

	default String parseCreateColumns(CreateCommand command) {
		return String.join(separator() + " ", command.getColumns() //
				.stream()
				.map(getTypeTranslator()::toSql)
				.toArray(String[]::new));
	}

	default String parseInsertColumns(ColumnsSelectableCommand<?> command) {
		String[] columns = command.getColumns();
		if (columns == null || columns.length == 0) {
			return "";
		}
		return "(" + parseColumns(command) + ")";
	}

	default String parseColumns(ColumnsSelectableCommand<?> command) {
		String[] columns = command.getColumns();
		if (columns == null || columns.length == 0) {
			return "*";
		}
		return String.join(separator() + " ", Arrays.stream(columns)
				.map(this::safeName)
				.toArray(String[]::new));
	}

	default String parseOrder(Order order) {
		if (!order.hasOrder()) {
			return "";
		}
		return " ORDER BY " +
				String.join(separator() + " ", Arrays.stream(order.getOrders())
						.map(concreteOrder -> safeName(concreteOrder.getColumn()) + " " + orderName(concreteOrder.getType()))
						.toArray(String[]::new));
	}

	default String parseInsertValues(RowCommand<?> command) {
		return "(" + parseValues(command) + ")";
	}

	default String parseValues(RowCommand<?> command) {
		return String.join(separator() + " ", command.getValues().values()
				.stream()
				.map(value -> {
					command.addParameter(value);
					return "?";
				}) //
				.toArray(String[]::new));
	}

	default String parseColumnValues(RowCommand<?> command) {
		return String.join(separator() + " ", command.getValues().entrySet().stream()
				.map(columnSet -> {
					command.addParameter(columnSet.getValue());
					return safeName(columnSet.getKey()) + " = ?";
				}) //
				.toArray(String[]::new));
	}

	default String parseLimit(LimitedCommand<?> command) {
		String ret = "";
		if (command.getRows() > 0) {
			ret += " LIMIT ";
			if (command instanceof OffsetCommand && ((OffsetCommand) command).getStart() > 0) {
				ret += ((OffsetCommand) command).getStart() + Character.toString(separator());
			}
			ret += command.getRows();
		}
		return ret;
	}

	@SuppressWarnings("deprecation")
	default String toSql(Command command) {
		CommandType.InnerType type = command.getType().getInnerType();
		switch (type) {
		case SELECT:
			return toSelectSql((SelectCommand) command);
		case UPDATE:
			return toUpdateSql((UpdateCommand) command);
		case DELETE:
			return toDeleteSql((DeleteCommand) command);
		case INSERT:
			return toInsertSql((InsertCommand) command);
		case CREATE:
			return toCreateSql((CreateCommand) command);
		case ALTER:
			return toAlterSql((AlterCommand) command);
		case DROP:
			return toDropSql((DropCommand) command);
		case TRUNCATE:
			return toTruncateSql((TruncateCommand) command);
		case GET_STRUCTURE:
			return toStructureSql(command);
		case UNKNOWN:
		default:
			return ((RawCommand) command).getRawSQL();
		}
	}

	/**
	 * SELECT {{distinct}} {{columns}} FROM {{table}} {{where}} {{order}} {{limit}}
	 */
	String getSelectFormat();

	default String toSelectSql(SelectCommand command) {
		String selectFormat = getSelectFormat();

		return selectFormat
				.replace("{{distinct}}", command.hasDupes() ? "" : "DISTINCT")
				.replace("{{columns}}", parseColumns(command))
				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{where}}", getWhereTranslator().parseWhere(command, command.getWhere()))
				.replace("{{order}}", parseOrder(command.getOrder()))
				.replace("{{limit}}", parseLimit(command))
				;
	}

	/**
	 * UPDATE {{table}} SET {{column-values}} {{where}} {{limit}}
	 */
	String getUpdateFormat();

	default String toUpdateSql(UpdateCommand command) {
		String updateFormat = getUpdateFormat();

		return updateFormat
				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{column-values}}", parseColumnValues(command))
				.replace("{{where}}", getWhereTranslator().parseWhere(command, command.getWhere()))
				.replace("{{limit}}", parseLimit(command))
				;
	}

	/**
	 * DELETE FROM {{table}} {{where}} {{limit}}
	 */
	String getDeleteFormat();

	default String toDeleteSql(DeleteCommand command) {
		String deleteFormat = getDeleteFormat();

		return deleteFormat
				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{where}}", getWhereTranslator().parseWhere(command, command.getWhere()))
				.replace("{{limit}}", parseLimit(command))
				;
	}

	/**
	 * INSERT INTO {{table}} {{column}} VALUES {{values}}
	 */
	String getInsertFormat();

	default String toInsertSql(InsertCommand command) {
		String insertFormat = getInsertFormat();

		return insertFormat
				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{column}}", parseInsertColumns(command))
				.replace("{{values}}", parseInsertValues(command))
				;
	}

	/**
	 * CREATE TABLE {{exists}} {{table}} {{columns-typed}}
	 */
	String getCreateFormat();

	default String toCreateSql(CreateCommand command) {
		String createFormat = getCreateFormat();

		return createFormat
				.replace("{{exists}} ", command.shouldIgnoreIfExists() ? "IF NOT EXISTS " : "")
				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{columns-typed}}", parseCreateColumns(command))
				;
	}

	/**
	 * ALTER TABLE {{table}} CHANGE COLUMN {{column}} {{column-typed}}
	 */
	String getAlterFormat();

	default String toAlterSql(AlterCommand command) {
		String alterFormat = getAlterFormat();

		return alterFormat
				.replace("{{table}}", safeName(command.getTable()))
				.replace("{{column}}", safeName(command.getOldColumnName()))
				.replace("{{column-typed}}", getTypeTranslator().toSql(command.getNewColumn()))
				;
	}

	/**
	 * DROP TABLE {{table}}
	 */
	String getDropFormat();

	default String toDropSql(DropCommand command) {
		String dropFormat = getDropFormat();

		return dropFormat
				.replace("{{table}}", safeName(command.getTable()))
				;
	}

	/**
	 * TRUNCATE {{table}}
	 */
	String getTruncateFormat();

	default String toTruncateSql(TruncateCommand command) {
		String truncateFormat = getTruncateFormat();

		return truncateFormat
				.replace("{{table}}", safeName(command.getTable()))
				;
	}

	// query should return column, type, max, default
	default String getStructureFormat() {
		return "SELECT column_name as col, data_type as typ, character_maximum_length as max_c, numeric_precision as max_n, numeric_scale as max_s, column_default as def " +
				"FROM INFORMATION_SCHEMA.COLUMNS " +
				"WHERE table_name = {{table}}";
	}

	default String toStructureSql(Command command) {
		return getStructureFormat()
				.replace("{{table}}", safeValue(command.getTable()))
				;
	}

	default Column[] reverse(ResultSet command) {
		List<Column> list = new LinkedList<>();
		Try.execute(() -> {
			while (command.next()) {
				String name = command.getString("col");
				Type type = getTypeTranslator().reverse(command.getString("typ"));
				String size = String.valueOf(command.getInt("max_c"));
				if (size.equals("0") && command.getInt("max_n") != 0) {
					size = command.getInt("max_n") + "." + command.getInt("max_s");
				}
				String defaultValue = command.getString("def");

				list.add(ColumnFactory.createFrom(name, type, size, defaultValue));
			}
		}).orThrow(RuntimeException::new);

		return list.toArray(new Column[0]);
	}
}
