package nl.makertim.sql.core.translator;

import nl.makertim.sql.core.types.Column;
import nl.makertim.sql.core.types.CustomColumn;

public interface SQLTypeTranslator extends TypeTranslator {

	CommandTranslator getTranslator();

	default String numberColumn(Column column) {
		double length = Double.parseDouble(column.getLength().replace(",", "."));
		if (length <= 0) {
			return "";
		}
		if (length % 1D == 0D) {
			return "(" + ((int) length) + ")";
		}
		return "(" + column.getLength() + ")";
	}

	@Override
	default String integerType(Column column) {
		return getTranslator().safeName(column.columnName()) + " INT" + numberColumn(column) + " " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String decimalType(Column column) {
		return getTranslator().safeName(column.columnName()) + " DECIMAL" + numberColumn(column) + " " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String charType(Column column) {
		return getTranslator().safeName(column.columnName()) + " CHAR" + numberColumn(column) + " " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String varCharType(Column column) {
		return getTranslator().safeName(column.columnName()) + " VARCHAR" + numberColumn(column) + " " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String textType(Column column) {
		return getTranslator().safeName(column.columnName()) + " TEXT " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String blobType(Column column) {
		return getTranslator().safeName(column.columnName()) + " BLOB " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String bigblobType(Column column) {
		return getTranslator().safeName(column.columnName()) + " LONGBLOB " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String dateType(Column column) {
		return getTranslator().safeName(column.columnName()) + " DATE " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String timeType(Column column) {
		return getTranslator().safeName(column.columnName()) + " TIME " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String yearType(Column column) {
		return getTranslator().safeName(column.columnName()) + " YEAR " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String datetimeType(Column column) {
		return getTranslator().safeName(column.columnName()) + " DATETIME " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String timestampType(Column column) {
		return getTranslator().safeName(column.columnName()) + " TIMESTAMP " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}

	@Override
	default String customType(CustomColumn column) {
		return getTranslator().safeName(column.columnName()) + " " + column.customName() + numberColumn(column) + " " + (column.defaultValue() == null ? "NULL" : column.defaultValue());
	}
}
