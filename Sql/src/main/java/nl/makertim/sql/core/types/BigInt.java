package nl.makertim.sql.core.types;

public class BigInt extends AbstractSimpleColumn {

	public BigInt(String columnName, String defaultValue) {
		super(columnName, 20, Type.INTEGER, defaultValue);
	}

}
