package nl.makertim.sql.core.types;

public class Bit extends AbstractSimpleColumn {

	public Bit(String columnName, String defaultValue) {
		super(columnName, 1, Type.INTEGER, defaultValue);
	}

}
