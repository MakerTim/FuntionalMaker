package nl.makertim.sql.core.types;

@Deprecated
@SuppressWarnings("DeprecatedIsStillUsed")
public class CustomColumn extends AbstractPreciseColumn {

	private String typeName;

	public CustomColumn(String columnName, String length, String type, String defaultValue) {
		super(columnName, length, Type.CUSTOM, defaultValue);
		typeName = type;
	}

	public String customName() {
		return typeName;
	}

}
