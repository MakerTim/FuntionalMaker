package nl.makertim.sql.core.types;

public class Decimal extends AbstractPreciseColumn {

	public Decimal(String columnName, String defaultValue) {
		super(columnName, "40,20", Type.DECIMAL, defaultValue);
	}

}
