package nl.makertim.sql.core.types;

public class Double extends AbstractPreciseColumn {

	public Double(String columnName, String defaultValue) {
		super(columnName, "20,10", Type.DECIMAL, defaultValue);
	}

	public Double(String columnName, String size, String defaultValue) {
		super(columnName, size.replace(".", ","), Type.DECIMAL, defaultValue);
	}

}
