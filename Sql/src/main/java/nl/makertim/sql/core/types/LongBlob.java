package nl.makertim.sql.core.types;

public class LongBlob extends Blob {

	public LongBlob(String columnName, String defaultValue) {
		super(columnName, defaultValue);
		length = 1;
	}

}
