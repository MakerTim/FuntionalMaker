package nl.makertim.sql.core.types;

public class TimeStamp extends AbstractSimpleColumn {

	public TimeStamp(String columnName, String defaultValue) {
		super(columnName, 0, Type.TIMESTAMP, defaultValue);
	}

}
