package nl.makertim.sql.core.types;

public enum Type {

	INTEGER,
	DECIMAL,
	CHAR,
	VARCHAR,
	TEXT,
	BLOB,
	TIMESTAMP,
	DATETIME,
	DATE,
	TIME,
	YEAR,
	CUSTOM

}
