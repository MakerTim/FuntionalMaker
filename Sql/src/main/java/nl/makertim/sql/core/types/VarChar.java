package nl.makertim.sql.core.types;

public class VarChar extends AbstractSimpleColumn {

	public VarChar(String columnName, String defaultValue) {
		super(columnName, 128, Type.VARCHAR, defaultValue);
	}

	public VarChar(String columnName, int length, String defaultValue) {
		super(columnName, length, Type.VARCHAR, defaultValue);
	}

}
