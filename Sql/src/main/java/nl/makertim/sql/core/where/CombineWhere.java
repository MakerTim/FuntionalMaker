package nl.makertim.sql.core.where;

public abstract class CombineWhere implements Where {

	private Where left;
	private CombineWhereType type;
	private Where right;

	public CombineWhere(Where left, CombineWhereType type, Where right) {
		this.left = left;
		this.type = type;
		this.right = right;
	}

	public Where getLeft() {
		return left;
	}

	public CombineWhereType getType() {
		return type;
	}

	public Where getRight() {
		return right;
	}
}
