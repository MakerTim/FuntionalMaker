package nl.makertim.sql.core.where;

public class CombineWhereAND extends CombineWhere {

	public CombineWhereAND(Where left, Where right) {
		super(left, CombineWhereType.AND, right);
	}
}
