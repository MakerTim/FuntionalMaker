package nl.makertim.sql.core.where;

public class CombineWhereOR extends CombineWhere {

	public CombineWhereOR(Where left, Where right) {
		super(left, CombineWhereType.OR, right);
	}
}
