package nl.makertim.sql.core.where;

public class CombineWhereXOR extends CombineWhere {

	public CombineWhereXOR(Where left, Where right) {
		super(left, CombineWhereType.XOR, right);
	}
}
