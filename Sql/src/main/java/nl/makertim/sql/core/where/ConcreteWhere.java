package nl.makertim.sql.core.where;

public abstract class ConcreteWhere implements Where {

	protected String column;
	protected ConcreteWhereType compare;
	protected Object value;

	ConcreteWhere(String column, ConcreteWhereType compare, Object value) {
		this.column = column;
		this.compare = compare;
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public ConcreteWhereType getCompare() {
		return compare;
	}

	public Object getValue() {
		return value;
	}
}
