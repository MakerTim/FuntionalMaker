package nl.makertim.sql.core.where;

public class WhereEquals extends ConcreteWhere {

	public WhereEquals(String column, Object value) {
		super(column, ConcreteWhereType.EQUALS, value);
	}
}
