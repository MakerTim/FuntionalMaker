package nl.makertim.sql.core.where;

public class WhereIn extends ConcreteWhere {

	protected Object[] values;

	public WhereIn(String column, Object... values) {
		super(column, ConcreteWhereType.IN, null);
		this.values = values;
	}

	@Override
	public Object getValue() {
		throw new IllegalArgumentException();
	}

	public Object[] getValues() {
		return values;
	}
}
