package nl.makertim.sql.core.where;

public class WhereSmaller extends ConcreteWhere {

	public WhereSmaller(String column, Object value) {
		super(column, ConcreteWhereType.SMALLER, value);
	}
}
