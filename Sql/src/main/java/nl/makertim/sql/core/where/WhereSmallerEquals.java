package nl.makertim.sql.core.where;

public class WhereSmallerEquals extends ConcreteWhere {

	public WhereSmallerEquals(String column, Object value) {
		super(column, ConcreteWhereType.SMALLER_EQUALS, value);
	}
}
