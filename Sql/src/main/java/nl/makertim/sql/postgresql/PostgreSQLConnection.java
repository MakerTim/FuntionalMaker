package nl.makertim.sql.postgresql;

import nl.makertim.functionalmaker.Try;
import nl.makertim.sql.core.Connection;
import nl.makertim.sql.core.translator.CommandTranslator;

import java.sql.ResultSet;

public class PostgreSQLConnection extends Connection {

	public PostgreSQLConnection(String server, int port, String username, String password, String database){
		this("postgresql", server, port, username, password, database);
	}

	protected PostgreSQLConnection(String type, String server, int port, String username, String password, String database) {
		super(type, server, port, username, password, database);
	}

	@Override
	protected String getDriverClass() {
		return "org.postgresql.Driver";
	}

	@Override
	@SuppressWarnings("deprecation")
	public String getServerVersion() {
		ResultSet rs = selectQuery(new nl.makertim.sql.core.command.RawCommand("SELECT version();"));
		return Try.execute(() ->
				(rs != null && rs.next()) ? rs.getString("version") : null
		).getData();
	}

	@Override
	protected CommandTranslator getNewTranslator() {
		return new PostgreSQLTranslator();
	}
}