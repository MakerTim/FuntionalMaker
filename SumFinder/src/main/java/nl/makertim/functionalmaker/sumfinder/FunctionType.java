package nl.makertim.functionalmaker.sumfinder;

import nl.makertim.functionalmaker.sumfinder.functions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FunctionType {
	protected static final List<FunctionType> values = new ArrayList<>();


	public static final FunctionType ADD = new FunctionType(new AddFunction());
	public static final FunctionType MINUS = new FunctionType(new MinusFunction());
	public static final FunctionType MULTIPLY = new FunctionType(new MultiplyFunction());
	public static final FunctionType POW = new FunctionType(new PowFunction());
	public static final FunctionType DIVIDE = new FunctionType(new DivideFunction());

	protected static FunctionType[] defaults = new FunctionType[]{ADD, MINUS, MULTIPLY, POW, DIVIDE};

	private Function input;

	protected FunctionType(Function input) {
		this.input = input;
		values.add(this);
	}

	public Function getFunction() {
		return input;
	}

	public static FunctionType[] values() {
		return values.toArray(new FunctionType[0]);
	}

	public static FunctionType[] defaults() {
		return defaults;
	}

	public static FunctionType[] defaultsWith(FunctionType... with) {
		List<FunctionType> inputTypeList = new ArrayList<>();
		inputTypeList.addAll(Arrays.asList(defaults()));
		inputTypeList.addAll(Arrays.asList(with));
		return inputTypeList.toArray(new FunctionType[0]);
	}
}
