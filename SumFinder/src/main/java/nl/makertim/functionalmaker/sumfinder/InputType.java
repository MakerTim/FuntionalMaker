package nl.makertim.functionalmaker.sumfinder;

import nl.makertim.functionalmaker.sumfinder.inputs.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputType {
	protected static final List<InputType> values = new ArrayList<>();

	public static final InputType N = new InputType(new Ns(-10, 10, 1));
	public static final InputType STATIC_NUMBERS = new InputType(new StaticNumbers(-15, 15, 1));
	public static final InputType START = new InputType(new StaticStart());
	public static final InputType NTHS = new InputType(new Nths(-3, -1, 1));
	public static final InputType PRIMES = new InputType(new Primes(-3, 3));
	public static final InputType SINGLE_RECURSION = new InputType(new TryEverythingAgain());

	protected static InputType[] defaults = new InputType[]{N, STATIC_NUMBERS, START, NTHS, PRIMES};

	private Input input;

	protected InputType(Input input) {
		this.input = input;
		values.add(this);
	}

	public Input getInput() {
		return input;
	}

	public static InputType[] defaults() {
		return defaults;
	}

	public static InputType[] values() {
		return values.toArray(new InputType[0]);
	}

	public static InputType[] defaultsWith(InputType... with) {
		List<InputType> inputTypeList = new ArrayList<>();
		inputTypeList.addAll(Arrays.asList(defaults()));
		inputTypeList.addAll(Arrays.asList(with));
		return inputTypeList.toArray(new InputType[0]);
	}
}
