package nl.makertim.functionalmaker.sumfinder;

import nl.makertim.functionalmaker.optional.UpdateAbleOptional;

import java.util.*;
import java.util.stream.Collectors;

public class Repetition {

	private List<Double> lastRepetitions = new ArrayList<>();
	private UpdateAbleOptional<RepetitionSet> solution = UpdateAbleOptional.empty();

	Repetition(double... input) {
		lastRepetitions.addAll(Arrays.stream(input).boxed().collect(Collectors.toList()));
	}


	public RepetitionSet getSolution() {
		return solution
				.orElseGetUpdate(() -> RepetitionFinder.findCombination(this));
	}

	public RepetitionSet getSolution(InputType... techniques) {
		return solution
				.orElseGetUpdate(() -> RepetitionFinder.findCombination(this, techniques));
	}

	public double findNext() {
		double answer = getSolution().getValue(new RepetitionContext(getKnownNumbers(), this));
		lastRepetitions.add(answer);
		return answer;
	}

	public final double[] findNext(int amount) {
		double[] next = new double[amount];
		for (int i = 0; i < amount; i++) {
			next[i] = findNext();
		}
		return next;
	}

	public Collection<Double> getKnownRepetitions() {
		return Collections.unmodifiableList(lastRepetitions);
	}

	int getKnownNumbers() {
		return lastRepetitions.size();
	}

	public double getStart() {
		return lastRepetitions.get(0);
	}

	public double getNth(int n) {
		return lastRepetitions.get(n);
	}
}
