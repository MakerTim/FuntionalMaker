package nl.makertim.functionalmaker.sumfinder;

import nl.makertim.functionalmaker.Foreach;
import nl.makertim.functionalmaker.property.BooleanProperty;
import nl.makertim.functionalmaker.property.ObjectProperty;
import nl.makertim.functionalmaker.sumfinder.inputs.Input;

import java.util.Arrays;

public class RepetitionFinder {

	private RepetitionFinder() {
	}

	public static RepetitionSet findCombination(Repetition repetition) {
		return findCombination(repetition, InputType.defaults());
	}

	public static RepetitionSet findCombination(Repetition repetition, InputType[] inputTypes) {
		return findCombination(repetition, inputTypes, FunctionType.defaults());
	}

	public static RepetitionSet findCombination(Repetition repetition, InputType[] inputTypes, FunctionType[] functionTypes) {
		ObjectProperty<RepetitionSet> answer = new ObjectProperty<>(null);
		Foreach.loopTwiceOrdered(inputTypes, (techniqueA, techniqueB) -> {
			techniqueA.getInput().handle(inputA -> {
				if (answer.isNotNull()) return;
				techniqueB.getInput().handle(inputB -> {
					if (answer.isNotNull()) return;
					runFunction(repetition, functionTypes, inputA, inputB, answer);
				});
			});
		});
		return answer.getValue();
	}

	private static void runFunction(Repetition repetition, FunctionType[] functionTypes, Input inputA, Input inputB, ObjectProperty<RepetitionSet> output) {
		Arrays.stream(functionTypes)
				.map(FunctionType::getFunction)
				.forEach(functionRaw -> {
					if (output.isNotNull()) {
						return;
					}
					functionRaw.handle(function -> {
						if (output.isNotNull()) {
							return;
						}
						RepetitionSet setToTry = new RepetitionSet(inputA, function, inputB);
						if (testSet(repetition, setToTry)) {
							output.setValue(setToTry);
						}
					});
				});
	}

	private static boolean testSet(Repetition repetition, RepetitionSet setToTry) {
		BooleanProperty isCorrect = new BooleanProperty(true);
		BooleanProperty hasFound = new BooleanProperty(false);
		RepetitionContext context = new RepetitionContext(0, repetition);
		testSet(1, context, setToTry, isCorrect, hasFound);
		if (isCorrect.getValue() && hasFound.getValue()) {
			testSet(0, context, setToTry, isCorrect, hasFound);
			return isCorrect.getValue() && hasFound.getValue();
		}
		return false;
	}

	private static void testSet(int start, RepetitionContext context, RepetitionSet setToTry, BooleanProperty isCorrect, BooleanProperty hasFound) {
		for (context.setN(start); isCorrect.getValue() && context.getN() < context.repetition.getKnownNumbers(); context.addOneN()) {
			try {
				double correctNumber = context.repetition.getNth(context.getN());
				double triedNumber = setToTry.getValue(context);
				isCorrect.setValue(triedNumber == correctNumber);
				hasFound.setValue(true);
			} catch (IndexOutOfBoundsException ignore) {
				hasFound.setValue(false);
			}
		}
	}
}
