package nl.makertim.functionalmaker.sumfinder;

public class SumFinder {

	private SumFinder() {
	}

	public static Repetition findRepetition(double d1, double d2, double d3, double... doubles) {
		double[] toArray = new double[doubles.length + 3];
		toArray[0] = d1;
		toArray[1] = d2;
		toArray[2] = d3;
		System.arraycopy(doubles, 0, toArray, 3, doubles.length);
		return new Repetition(toArray);
	}

}
