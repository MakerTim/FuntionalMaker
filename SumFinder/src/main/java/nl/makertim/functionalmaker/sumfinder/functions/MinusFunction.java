package nl.makertim.functionalmaker.sumfinder.functions;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;
import nl.makertim.functionalmaker.sumfinder.inputs.Input;

public class MinusFunction implements Function {

	@Override
	public double applyAsDouble(Input input, Input input2, RepetitionContext context) {
		return input.getValue(context) - input2.getValue(context);
	}

	@Override
	public String toString() {
		return "-";
	}
}
