package nl.makertim.functionalmaker.sumfinder.functions;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;
import nl.makertim.functionalmaker.sumfinder.inputs.Input;

public class PowFunction implements Function {

	@Override
	public double applyAsDouble(Input input, Input input2, RepetitionContext context) {
		return Math.pow(input.getValue(context), input2.getValue(context));
	}

	@Override
	public String toString() {
		return "^";
	}
}
