package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Ns extends VariableInput {

	private static N[] inputsBetween(int min, int max, int step) {
		List<N> input = new ArrayList<>();
		for (int i = max; i >= min; i -= step) {
			input.add(new N(i));
		}
		return input.stream()
				.sorted(Comparator.comparingDouble(n -> Math.abs(n.d)))
				.toArray(N[]::new);
	}

	public Ns(int min, int max, int step) {
		super(inputsBetween(min, max, step));
	}

	@Override
	public String getCollectionName() {
		return "Ns";
	}

	private static class N implements Input {
		private double d;

		public N(int d) {
			this.d = d;
		}

		@Override
		public double getValue(RepetitionContext context) {
			return context.getN() + d;
		}

		@Override
		public String toString() {
			if (d == 0) {
				return "N";
			}
			if (d > 0) {
				return "N+" + d;
			}
			return "N" + d;
		}
	}
}
