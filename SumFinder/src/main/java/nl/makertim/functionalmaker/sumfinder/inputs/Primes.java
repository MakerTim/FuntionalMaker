package nl.makertim.functionalmaker.sumfinder.inputs;

import nl.makertim.functionalmaker.sumfinder.RepetitionContext;

import java.util.stream.IntStream;

public class Primes extends VariableInput {

	private static double[] firstPrimes = {
			2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
			43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,
			101, 103, 107, 109, 113, 127, 131, 137, 139, 149,
			151, 157, 163, 167, 173, 179, 181, 191, 193, 197,
			199, 211, 223, 227, 229, 233, 239, 241, 251, 257,
			263, 269, 271, 277, 281, 283, 293, 307, 311, 313,
			317, 331, 337, 347, 349, 353, 359, 367, 373, 379,
			383, 389, 397, 401, 409, 419, 421, 431, 433, 439,
			443, 449, 457, 461, 463, 467, 479, 487, 491, 499,
			503, 509, 521, 523, 541
	};




	private static Prime[] firstPrimes(int offset, int endOffset) {
		return IntStream.range(offset, endOffset + 1)
				.mapToObj(Prime::new).toArray(Prime[]::new);
	}

	public Primes(int offset, int endOffset) {
		super(firstPrimes(offset, endOffset));
	}

	@Override
	public String getCollectionName() {
		return "Primes";
	}

	static class Prime implements Input {
		private final int i;

		private static boolean bruteForceIsPrime(int number) {
			for (int i = 2; i < number; i++) {
				if (number % i == 0) {
					return false;
				}
			}
			return true;
		}

		public Prime(int offset) {
			this.i = offset;
		}

		@Override
		public synchronized double getValue(RepetitionContext context) {
			int index = context.getN() + i;
			while (index >= Primes.firstPrimes.length) {
				double[] newCache = new double[Primes.firstPrimes.length + 10];
				System.arraycopy(Primes.firstPrimes, 0, newCache, 0, Primes.firstPrimes.length);
				for (int newPrimeCount = 0; newPrimeCount < 10; newPrimeCount++) {
					double latestPrime = newCache[Primes.firstPrimes.length - 1 + newPrimeCount];
					for (int n = 1; n < latestPrime; n++) {
						if (bruteForceIsPrime((int) latestPrime + n)) {
							newCache[Primes.firstPrimes.length + newPrimeCount] = latestPrime + n;
							break;
						}
					}
				}
				Primes.firstPrimes = newCache;
			}
			return Primes.firstPrimes[context.getN() + i];
		}

		@Override
		public String toString() {
			if (i == 0) {
				return "Prime(N)";
			}
			if (i > 0) {
				return "Prime(N+" + i + ")";
			}
			return "Prime(N" + i + ")";
		}
	}
}
