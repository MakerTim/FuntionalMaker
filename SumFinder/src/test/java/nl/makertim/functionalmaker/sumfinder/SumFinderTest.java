package nl.makertim.functionalmaker.sumfinder;

import org.junit.Assert;
import org.junit.Test;

public class SumFinderTest {

	@Test
	public void findsAdd() {
		double a1 = 1;
		double a2 = 2;
		double a3 = 3;

		Repetition repetition = SumFinder.findRepetition(a1, a2, a3);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNotNull(solution);

		Assert.assertEquals(4, repetition.findNext(), 0);
		Assert.assertEquals(5, repetition.findNext(), 0);
		Assert.assertEquals(6, repetition.findNext(), 0);
		Assert.assertEquals(7, repetition.findNext(), 0);
	}

	@Test
	public void findsCube() {
		double a1 = 1;
		double a2 = 2;
		double a3 = 4;
		double a4 = 8;

		Repetition repetition = SumFinder.findRepetition(a1, a2, a3, a4);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNotNull(solution);

		Assert.assertEquals(16, repetition.findNext(), 0);
		Assert.assertEquals(32, repetition.findNext(), 0);
		Assert.assertEquals(64, repetition.findNext(), 0);
	}

	@Test
	public void findsN() {
		double a1 = 1;
		double a2 = 2;
		double a3 = 4;
		double a4 = 7;

		Repetition repetition = SumFinder.findRepetition(a1, a2, a3, a4);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNotNull(solution);

		Assert.assertEquals(11, repetition.findNext(), 0);
		Assert.assertEquals(16, repetition.findNext(), 0);
		Assert.assertEquals(22, repetition.findNext(), 0);
	}

	@Test
	public void findsFibonacci() {
		double a1 = 0;
		double a2 = 1;
		double a3 = 1;
		double a4 = 2;
		double a5 = 3;

		Repetition repetition = SumFinder.findRepetition(a1, a2, a3, a4, a5);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNotNull(solution);

		double next = repetition.findNext();
		// If next is 16, its also correct! but not fibonacci
		if (next == 16D) {
			Assert.assertEquals(16, next, 0);
			Assert.assertEquals(125, repetition.findNext(), 0);
			Assert.assertEquals(2.821109907456E12, repetition.findNext(), 0);
		} else {
			Assert.assertEquals(5, next, 0);
			Assert.assertEquals(8, repetition.findNext(), 0);
			Assert.assertEquals(13, repetition.findNext(), 0);
		}
	}

	@Test
	public void findsFibonacciStronk() {
		double a1 = 0;
		double a2 = 1;
		double a3 = 1;
		double a4 = 2;
		double a5 = 3;
		double a6 = 5;

		Repetition repetition = SumFinder.findRepetition(a1, a2, a3, a4, a5, a6);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNotNull(solution);

		Assert.assertEquals(8, repetition.findNext(), 0);
		Assert.assertEquals(13, repetition.findNext(), 0);
		Assert.assertEquals(21, repetition.findNext(), 0);
	}

	@Test
	// http://mathforum.org/library/drmath/view/61338.html
	public void internetSequence() {
		Repetition repetition = SumFinder.findRepetition(1, 7, 23, 55, 109);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution(InputType.defaultsWith(InputType.SINGLE_RECURSION));
		Assert.assertNotNull(solution);

		Assert.assertEquals(191, repetition.findNext(), 0);
		Assert.assertEquals(307, repetition.findNext(), 0);
		Assert.assertEquals(463, repetition.findNext(), 0);
	}

	@Test
	// Discord 9Y0#8995
	public void simcha() {
		Repetition repetition = SumFinder.findRepetition(1, 1, 2, 6, 24, 120, 720);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNotNull(solution);

		Assert.assertEquals(5040, repetition.findNext(), 0);
		Assert.assertEquals(40320, repetition.findNext(), 0);
	}

	@Test
	public void primeFinds() {
		Repetition repetition = SumFinder.findRepetition(2, 3, 5, 7, 11, 13);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNotNull(solution);

		Assert.assertEquals(17, repetition.findNext(), 0);
		Assert.assertEquals(19, repetition.findNext(), 0);
		Assert.assertEquals(23, repetition.findNext(), 0);
	}

	@Test
	public void mathopolis() {
		Repetition repetition;
		RepetitionSet solution;

		// https://www.mathopolis.com/questions/q.php?id=3900
		repetition = SumFinder.findRepetition(2, 7, 16, 30, 50, 77);
		solution = repetition.getSolution(InputType.defaultsWith(InputType.SINGLE_RECURSION));
		Assert.assertNotNull(solution);

		// https://www.mathopolis.com/questions/q.php?id=3897
		repetition = SumFinder.findRepetition(0, 2, 5, 9);
		solution = repetition.getSolution();
		Assert.assertNotNull(solution);
		Assert.assertEquals(14, repetition.findNext(), 0);

		// https://www.mathopolis.com/questions/q.php?id=3898
		repetition = SumFinder.findRepetition(0, 2, 6, 12, 20);
		solution = repetition.getSolution(InputType.defaultsWith(InputType.SINGLE_RECURSION));
		Assert.assertNotNull(solution);
		Assert.assertEquals(30, repetition.findNext(), 0);

		// https://www.mathopolis.com/questions/q.php?id=1245
		repetition = SumFinder.findRepetition(1, 5, 12, 22, 35);
		solution = repetition.getSolution(InputType.defaultsWith(InputType.SINGLE_RECURSION));
		Assert.assertNotNull(solution);
		double next = repetition.findNext();
		Assert.assertTrue(next == 60D || next == 51D);
	}

	@Test
	public void nowayRepetition() {
		Repetition repetition = SumFinder.findRepetition(0, 1, 2, 3, 4, 128);
		Assert.assertNotNull(repetition);

		RepetitionSet solution = repetition.getSolution();
		Assert.assertNull(solution);
	}
}
